/*
 * Util.cpp
 *
 * Created: 6/2/2017 11:51:15
 *  Author: Fredc
 */ 

//#include "Config.h"
#include <stdlib.h>
#include "Util.h"

  //bool Util::Debug_Flag = false;
  //char * Util::Debug_String = "DEBUG";
    
   int Util::Bracket (int x, int high, int low)
      {
      if (x > high) return high;
      if (x < low) return low;
      return x;
      }

   double Util::Bracket (double x, double high, double low)
      {
      if (x > high) return high;
      if (x < low) return low;
      return x;
      }
      
      
   double Util::Minimum (double x, double min)
      {
      if (x < min) return x;
      return min;
      }
      
   double Util::Maximum (double x, double max)
      {
      if (x > max) return x;
      return max;
      }
      
 char Util::ToUpper (char c)
    {
    char cc;
    
    cc = c & 0x7f;
    if ((cc >= 'a') && (cc <= 'z')) return (cc & ~0x20); 
    return cc ;
    }

 int Util::Abs (int A)
    {
    if (A < 0) return -A;
    return A;
    }

//=======================
   
     
   CBox::~CBox ( )
      {
      free (buffer); 
      }

   void CBox::CBox_Init (int b_size)
      {
      int i;
      
      buffer = (char *) malloc (b_size);
      buffer_size = b_size; 
      for (i = 0; i < b_size; i++)
         buffer [i] = 0;
      incounter = 0;
      outcounter = 0;
      }
//
   bool CBox::Ready ( )                 // Check for messages in buffer
      {
      if (incounter == outcounter)
         return false;
      return true;
      }

   int CBox::Put_Char (char ch)
      {
      incounter += 1;
      if (incounter > buffer_size - 1) incounter = 0;
      buffer [incounter] =  ch;
      return incounter;                    //
      }

   char CBox::Get_Char ( )
      {
      char ch;
      
      outcounter += 1;
      if (outcounter > buffer_size - 1) outcounter = 0; 
      ch = buffer [outcounter];     
      return ch;
      }


