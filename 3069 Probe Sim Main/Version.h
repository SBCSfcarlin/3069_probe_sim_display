
#ifndef __VERSION_H__
#define __VERSION_H__

#if __cplusplus
extern "C" {
#endif

#define SG_VERSION SG_B

//ver. 4.01 B
//8-18-11 ph timer alarm move dec. point
//8-15-11 startup delay for adc
//8-17-11 add BYPASS FAIL eAlarms

//ver. 4.01 A
//4-22-11 use chan 0 for ph
//3-9-11 ADD BYPASS ALARM RLY

//ver. 2.66 F
//11-01-10 PH_LOW_LIMIT_DEFAULT fr. 600 back to 700

//ver. 2.66 E
//Hires OR normal version
//3-15-10 fix reset PPM w/ BOTH PPM and PH buttons held down for 6 sec.

//ver. 2.66 c
//2-5-10 dis. SetPPMlvl()
//2-4-10 PH_SAFETY_TIME_DEFAULT = 5
//2-4-10 mod. check_cal_brd() to skip ppm cal for now
//2-4-10 ORP_SAFETY_TIME_DEFAULT = 15
//2-4-10 add back ph interlock

//ver. 2.66 
//1-12-10 default PPM calibration w/ PPM key
//10-31-09 add eAlarms
//10-29-09 new PPM probe

//ver. 2.65
//10-15-08 update all Fail(x) +/- 10% tol.
//3-11-08 update 4-20ma for ph 4-20ma display
//6-23-05 update default PPM calibration
//6-22-05 #ifdef Hires 0 to 4.00 PPM
//6-21-05 default interlock OFF
//6-21-05 ORP_SAFETY_TIME_DEFAULT fr. 15 to 0
//6-21-05 PH_SAFETY_TIME_DEFAULT fr. 5 to 0
//6-21-05 PH_LOW_LIMIT_DEFAULT fr. 700 to 600
//6-21-05 ORP_SETPOINT_INC fr. 5 to 1

//3-11-05 add PPM_DEBUG

//3-7-05 add check_420_calbrd()
//3-1-05 for PPM DEBUG 
//2-25-05 fix slave_wr_ee_tst
//2-22-05 add 4-20ma output opt.
//2-22-05 change PPM default high limit to 3.0
//this is for 4-20ma output version

//9-13-04 new 240 model opt. (define opt240)
//opt240 = Ph only
//opt230 = ORP only

//ver 4.50
//8-24-04 fix SG_G
//8-24-04 DispPPMlvl() BACK to 1,2,4
//8-23-04 update version to 4.50(255b) & 4.00 (250b)
//8-23-04 "PPN" to "GF" (gain factor)

//ver 3.09
//8-16-04 UPDATE setup display
//8-06-04 add TIMEOUT for system setup
//8-3-04 add UP/DN = system setup
//7-22-04 ORP cal. 250-800
//7-21-04 add Fail(fno) error msgs

//ver 3.08
//add AVEARRAYSIZE = 25 (more averaging)
//7-20-04 add Dispemsg()
//7-20-04 make 10 sec. running ave.
//7-20-04 startup dly before isr_svc_adc_scanner()

//ver 3.07
//7-13-04 ALLOW ORP key at power up to reset PPM Probe ch.1
//7-13-04 put back set_gPpmGain
//7-12-04 fix ph offset cal. 
//7-8-04 move E8 jmp startup SPIdis

//ver 3.06
//6-29-04 remove adc_filtered
//6-28-04 allow user partial factory default settings_reset_orp() etc.,
//6-28-04 SET DEFAULT TO PP1
//6-28-04 use get_ave to filter readings

//ver 3.05
//6-15-04 dis. ORP key at power up to calibrate PPM Probe
//6-15-04 add watchdog
//6-11-04 disable display_startup_info printout (sends garbage??)
//6-10-04 add user PPM calibration
//5-26-04 allow ORP key at power up to calibrate PPM Probe
//5-25-04 revise calibration
//5-24-04 add PPM chan. selection 1x, 2x, 4x
//5-24-04 remove gPpmAve (use gOrpAve)
//5-24-04 #define PPMopt in project opt.
//5-24-04 init gPhAve & gOrpAve vars at startup
//5-22-04 add slave_wr_ee_tst()
//5-18-04 add vane type FLOW sw. for FLOW
//5-12-04 at startup display PaddleWheel 1 or 0
//5-6-04 revise LED outputs
//5-6-04 add SetPhLock() SetPaddleW()
//5-5-04 add PPM_DP compile sw. for ORP/PPM
//5-3-04 ave ph reading
//4-29-04 add gcmd[]
//5-3-04 MOVE paddle wheel FR.PIN4.2 TO ACID/BASE TYPE PROG EN.
//4-16-04 add gSPI
//4-15-04 add E8 jmp startup SPIdis INPUT (PORTG.4)
//4-15-04 add E5 test INPUT (PORTF.1)
//4-13-04 8.0 Mhz update
//4-9-04 add calibration
//4-9-04 add E10 test OUTPUT (PORTB.0)
//4-9-04 add E4 test OUTPUT (PORTF.0)
//4-9-04 add gdispen
// add DEBUG on Compiler Tab -> Macro Defines for this and other debug options
//4-9-04 add second UART
//03-25-04 PORTB.4 = FLOW input
//3-25-04 revise for atmega128 init.
//03-23-04 revise A/D spi interface
//03-23-04 INVERT RELAY OUTPUTS PORTB.6-7, PORTC.7
//02-25-04 invert PORTC.0 to .6 outputs
//02-25-04 MOVE pe.3 & 4 to pd.3 & 2 position

//ver 1.03 PPM 0-10.9 ppm (+vdc to base jumper)
//12-5-03 fix PPM prop. and safety timer (no decimal point)
//12-5-03 fix PPM defaults

//ver 1.02 PPM 0-10.9 ppm (+vdc to base jumper)
//9-15-03 fix PPM calibration

//ver 1.01 PPM 0-10.9 ppm (+vdc to base jumper)
//9-8-03 0-10.9 ppm range
//ver 1.00 PPM 0-9.99 ppm (+vdc to base jumper)
//9-5-03 add diag adj 5.00 vdc for JP14.3 calc.
//9-4-03 use ACID/BASE jumper JP14.3 ADMUX ch 0 to read PPM fr. Ted's board
//9-4-03 change ORP defaults to PPM levels
//9-4-03 allow ph key at power up to program ACID/BASE
/*
add diag entry of user_set.orp.adcvolts (500 = 5.00 vdc) for adc calibration.
set to measured 5vdc 
  to enter power up w/ UP & DOWN pressed.
  then scroll to AdC then press ORP then UP/DOWN then ORP when done.

w/ running ave. of 10 ad chan 0.  Can adj offset user_set.orp.offset
has gain of 1.000 (can't adj. except in main())
*/

//ver 3.00
//Hi-res ver. 3.00 program to read PPM via ORP channel 0.00 to 2.00 ppm
//8-22-03 add PPM fr. 0.00 to 2.00 range on ORP input
//ver 2.02
//12-11-02 allow ph 0-14
//12-11-02 allow ORP 0-995
#ifdef PPMopt
#define SW_VERSION      451
#else
#define SW_VERSION      401	
#endif	
//9-13-04 new 240 model opt. (define opt240)
#ifdef opt240
#undef SW_VERSION
#define SW_VERSION      441	
#endif
#ifdef opt230
#undef SW_VERSION
#define SW_VERSION      431	
#endif


                                //300 = 8-22-03 add PPM fr. 0.00 to 2.00 								  	
						   		//202 = 12-11-02										  
						        //201 - 3-20-01												  
						    	//109 = 2-24-01
						        /* 1.00==40 */
                                /* 1.00 = Dec 1 */
                                /* 1.01 = Dec 27 */
                                /* 1.02 = Wed  01-26-00  15:21:31 */
                                /* 1.03 = Fri  02-25-00  15:54:49 */
                                /* 1.04 = Sun  04-02-00  16:49:29 */
                                /* 1.05 = Thu  04-06-00  16:02:20 Jacques */
                                /* 1.06 = Fri  04-07-00  10:45:01 Jacques - settings default */
                                /* 1.07 = Sun  08-20-00  23:38:16 Pablo - blinking control values out of range */
                                /* 1.08 = Fri  09-01-00  14:25:09 Pable - blink fixe & tracking */

/************************************************************************/
/*                                                                      */
/************************************************************************/

#if __cplusplus
}
#endif  /* __cplusplus */

#endif  /* __VERSION_H__ */


