/*
 * Display.cpp
 *
 * Created: 7/31/2017 10:38:48
 *  Author: Fredc
 */ 
   #include <avr/io.h> 
   
   #include "Config.h"
   #include "Display.h"
   #include "Util.h"

   //int CDisplay::chdata [8];
   //int CDisplay::chcurr;             // Current character
   //
   //static bool San_Feed;
   //
   void CDisplay::Init ( )            // Initialize Display
      {
      //chcurr = 0;
      //San_Feed = false;
      //}
      //
   //void CDisplay::Disp_pH (double pH)
      //{
      //int Du;        // Digit Units
      //int Dt;        // Digit tenths
      //
      //int ipH;
      //
      //ipH = (int) (10.0 * (pH + 0.05));
      //ipH = Util::Bracket (ipH, 140, 0);
      //
      //if (ipH < 100)
         //{        
         //Dt  = ipH % 10;
         //ipH = ipH / 10;
         //Du  = ipH % 10;
      //
         //Disp_Put (3, Du, true);
         //Disp_Put (4, Dt, false);
         //}
         //else
         //{
         //Du = (int) (pH + 0.5) % 10; 
         //Dt = 1;
         //Disp_Put (3, Dt, false);
         //Disp_Put (4, Du, false);
         //}
               //
      }

   
          
   //void CDisplay::Disp_ORP (double ORP)
      //{
      //int Dh;
      //int Dt;
      //int Du;
      //
      //int iORP;
      //
      //iORP = (int) ORP;
      //Du = iORP % 10;
      //iORP = iORP / 10;
      //Dt = iORP % 10;
      //iORP = iORP / 10;
      //Dh = iORP % 10;
      //
      //Disp_Put (0, Dh, false);
      //Disp_Put (1, Dt, false);
      //Disp_Put (2, Du, false);
      //}
     //
     
  //void CDisplay::Disp_HH (double HH) 
      //{
      //int Dh;
      //int Dt;
      //int Du;
      //
      //int iHH;
      //
      //iHH = (int) HH;
      //Du = iHH % 10;
      //iHH = iHH / 10;
      //Dt = iHH % 10;
      //iHH = iHH / 10;
      //Dh = iHH % 10;
      //
      ////Disp_Put (0, Dh, false);
      //Disp_Put (3, Dt, false);
      //Disp_Put (4, Du, false);
      //}
  //
     //
      //
  //void CDisplay::Disp_pH (char *str)        // Display pH
     //{
     //Disp_Put (3, str [0], false);
     //Disp_Put (4, str [1], false);  
     //}
//
   //void CDisplay::Disp_ORP (char *str)       // Display ORP
     //{
     //Disp_Put (0, str [0], false);
     //Disp_Put (1, str [1], false);
     //Disp_Put (2, str [2], false);  
     //}        
     
      
   //=============================================================    
      
   //void CDisplay::Disp_Put (int position, char ch, bool dp)
      //{
      //int pos;
      //int seg;
      //
      //pos = Util::Bracket (position, 6, 0);
      //switch (ch)
         //{
         //case    0: seg = 0x3f; break;    // III:120
         //case    1: seg = 0x06; break; 
         //case    2: seg = 0x5b; break; 
         //case    3: seg = 0x4f; break; 
         //case    4: seg = 0x66; break; 
         //case    5: seg = 0x6d; break; 
         //case    6: seg = 0x7d; break; 
         //case    7: seg = 0x07; break; 
         //case    8: seg = 0x7f; break; 
         //case    9: seg = 0x67; break; 
         //case 0x0a: seg = 0x3f; break; 
         //case 0x0b: seg = 0x7c; break; 
         //case 0x0c: seg = 0x58; break; 
         //case 0x0d: seg = 0x5e; break; 
         //case 0x0e: seg = 0x7d; break; 
         //case 0x0f: seg = 0x71; break; 
         //case '0':  seg = 0x3f; break;
         //case '1':  seg = 0x06; break;
         //case '2':  seg = 0x5b; break;
         //case '3':  seg = 0x4f; break;
         //case '4':  seg = 0x66; break;
         //case '5':  seg = 0x6d; break;
         //case '6':  seg = 0x7d; break;
         //case '7':  seg = 0x07; break;
         //case '8':  seg = 0x7f; break;
         //case '9':  seg = 0x67; break;
         //
         //case 'a':  seg = 0x5f; break;
         //case 'b':  seg = 0x7d; break;
         //case 'c':  seg = 0x58; break;
         //case 'd':  seg = 0x5e; break;
         //case 'e':  seg = 0x7b; break;
         //case 'f':  seg = 0x71; break;
         //case 'i':  seg = 0x10; break; 
         //case 'o':  seg = 0x5c; break;
         //case 'l':  seg = 0x30; break;  
         //case 'r':  seg = 0x50; break;
         //case 'A':  seg = 0x77; break;
         //case 'B':  seg = 0xff; break;
         //case 'C':  seg = 0x39; break;
         //case 'E':  seg = 0x79; break;
         //case 'F':  seg = 0x71; break;
         //case '-':  seg = 0x40; break;
         //case '=':  seg = 0x41; break;
         //case '#':  seg = 0x49; break;    // Triple dash
         //case ' ':  seg = 0x00; break;
         //case 'I':  seg = 0x06; break;
         //case 'L':  seg = 0x38; break;
         //case 'H':  seg = 0x76; break;
         //case 'O':  seg = 0x3f; break;
         //case 'P':  seg = 0x73; break;
         //case 'S':  seg = 0x6d; break;
         //case 't':  seg = 0x78; break;
         //default:   seg = 0x80; break; 
         //}
      //if (dp) seg |= 0x80;
      //chdata [pos] = seg;                //Util::Bracket (ch, 0x7f, 0);
      //}
   //
   //void CDisplay::Set_San_Feed (bool On_Off)
      //{
      //San_Feed = On_Off;  
      //}
      //
   //void CDisplay::Disp_UPD ( )            // Update Display at refresh rate;
      //{
      ////int B7;        // Bit 7
      //int PC;        // Port C
      //
     //
      //PC = 0;
      //chcurr += 1;                     // Bump up refresh character
      //if (chcurr > 6) chcurr = 0;     // Wrap. 5 digits plus 2 groups
      //
      ////B7 = PORTC & BIT_7;              // Use instead of PINC 
      //PORTA = chdata [chcurr];         // Get data to display
      //
      //switch (chcurr)
         //{
         //case 0: PC = ~BIT_2; break;
         //case 1: PC = ~BIT_1; break;
         //case 2: PC = ~BIT_0; break;
         //case 3: PC = ~BIT_4; break;
         //case 4: PC = ~BIT_3; break;
         //case 5: PC = ~BIT_5; break;
         //case 6: PC = ~BIT_6; break;
         //default: PC = 0x7f;  break;
         //} 
      //if (San_Feed) PC &= ~0x80; else PC |= 0x80;
      //PORTC =  PC;   
      //}
      
      //0x3f,   /* '0' 0  */
      //0x06,   /* '1' 1  */
      //0x5b,   /* '2' 2  */
      //0x4f,   /* '3' 3  */
      //0x66,   /* '4' 4  */
      //0x6d,   /* '5' 5  */
      //0x7d,   /* '6' 6  */
      //0x07,   /* '7' 7  */
      //0x7f,   /* '8' 8  */
      //0x6f,   /* '9' 9  */
      //0x77,   /* 'A' 10 */
      //0x7c,   /* 'b' 11 */
      //0x39,   /* 'C' 12 */
      //0x5e,   /* 'd' 13 */
      //0x79,   /* 'E' 14 */
      //0x71,   /* 'F' 15 */
      //0x7d,   /* 'G' 16 */ //8-24-04 fix SG_G
      //0x76,   /* 'H' 17 */
      //0x06,   /* 'I' 18 */
      //0x1E,   /* 'J' 19 */
      //0x38,   /* 'L' 20 */
      //0x73,   /* 'P' 21 */
      //0x50,   /* 'r' 22 */
      //0x6D,   /* 'S' 23 */
      //0x44,   /* 't' 24 */
      //0x3E,   /* 'U' 25 */
      //0x80,   /* DP  26 */
      //0x53,   /* '?' 27 */
      //0x40,   /* '-' 28 */
      //0x08,   /* '_' 29 */
      //0x01,   /* BAR 30 */    /* a segment lit */
      //0x00,    /* SP  31 */
      //0x37    /* 'n' 32 */  //8-16-04 UPDATE setup display
      //};
           // int static pH_Status_Blink;
           // int static ORP_Status_Blink;
            // int static pH_Status;
            // int static ORP_Status_Blink;
            
       #define pFeed 0x01
       #define pAlar 0x02 
       #define pMan  0x04 
       #define pOff  0x08      
       #define pAuto 0x10 
       
       #define oAuto 0x01
       #define oMan  0x02
       #define oOff  0x04
       #define oAlar 0x08
       #define oFeed 0x10
       
       
       //void CDisplay::LED_Auto   (Probe_Type probe, bool on_off, bool blink) 
          //{
          //switch (probe)
             //{
             //case Probe_None: break;
             //case Probe_pH:  on_off ? chdata[6] |= pAuto  : chdata[6] &= ~pAuto; break;
             //case Probe_ORP: on_off ? chdata[5] |= oAuto  : chdata[5] &= ~oAuto; break;
             //}   
          //}
       //
       //void CDisplay::LED_Off    (Probe_Type probe, bool on_off, bool blink)
          //{
           //switch (probe)
              //{
              //case Probe_None: break;
              //case Probe_pH:  on_off ? chdata[6] |= pOff   : chdata[6] &= ~pOff; break;
              //case Probe_ORP: on_off ? chdata[5] |= oOff   : chdata[5] &= ~oOff; break;
              //}
          //}
          //
       //void CDisplay::LED_Man    (Probe_Type probe, bool on_off, bool blink)
          //{
          //switch (probe)
             //{
             //case Probe_None: break;
             //case Probe_pH:  on_off ? chdata[6] |= pMan   : chdata[6] &= ~pMan; break;
             //case Probe_ORP: on_off ? chdata[5] |= oMan   : chdata[5] &= ~oMan; break;
              //}
          //}
       //
       //
       //void CDisplay::Disp_Mode (Probe_Type probe, Run_Mode mode)
          //{
         //switch (mode)
            //{
            //case Off_Mode:
               //LED_Off  (probe, true,  false);
               //LED_Man  (probe, false, false);
               //LED_Auto (probe, false, false);
               //break;
            //case Man_Mode:
               //LED_Off  (probe, false, false);
               //LED_Man  (probe, true,  false);
               //LED_Auto (probe, false, false);
               //break;
            //case Auto_Mode:
               //LED_Off  (probe, false, false);
               //LED_Man  (probe, false, false);
               //LED_Auto (probe, true,  false);
               //break;
            //}            
         //}
         //
   //
          //
          //
       //void CDisplay::LED_Feed   (Probe_Type probe, bool on_off, bool blink)       
          //{
          //switch (probe)
             //{
             //case Probe_None: break;
             //case Probe_pH:  on_off ? chdata[6] |= pFeed   : chdata[6] &= ~pFeed; break;
             //case Probe_ORP: on_off ? chdata[5] |= oFeed   : chdata[5] &= ~oFeed; break;
             //}
          //}
          //
          //
       //void CDisplay::LED_Alarm  (Probe_Type probe, bool on_off, bool blink)
          //{
          //switch (probe)
             //{
             //case Probe_None: break;
             //case Probe_pH:  on_off ? chdata[6] |= pAlar   : chdata[6] &= ~pAlar; break;
             //case Probe_ORP: on_off ? chdata[5] |= oAlar   : chdata[5] &= ~oAlar; break;
             //}
          //}
       //
    