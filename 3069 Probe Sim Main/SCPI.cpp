/*
 * SCPI.cpp
 *
 * Created: 10/24/2017 13:03:34
 *  Author: Fredc
 */ 


 #include <stdio.h>

 #include "Controller.h"
 #include "Util.h"
 
 #include "PCBA_SIM18.h"
  
 #include "SCPI.h"
 


 
    void CSCPI::SCPI_Init (void)
       {
       SCPI_Chemtrol_Flag = false;			// In Chemtrol mode
       SCPI_ORP_Flag      = false;			// In ORP Mode
       SCPI_pH_Flag       = false;			// in pH Mode
	   SCPI_PPM_Flag      = false;			// In PPM Mode
       }

//--------------------------------------------------------------

    void CSCPI::SCPI_Put_pH (double pH)
       {
       char str [20];
       int pHu;
       int pHl;
       int pHi;
       
       pHi = (int) (pH * 10.0);
       pHl = pHi % 10;
       pHu = pHi / 10;
       sprintf (str, "%d.%d\r\n",pHu, pHl);
       //PCBA_CH250K::RS232_Send (str);
       }
    
    void CSCPI::SCPI_Put_Feed (void)
       {
       if (SCPI_pH_Flag)
          {
          //if (pH_Sensor.Get_Reverse_Feed ( )) PCBA_CH250K::RS232_Send ("ACID\r\n");
          //else PCBA_CH250K::RS232_Send ("BASE\r\n");
          }
       if (SCPI_ORP_Flag)
          {
          //if (ORP_Sensor.Get_Reverse_Feed ( )) PCBA_CH250K::RS232_Send ("REDU\r\n");
             //else PCBA_CH250K::RS232_Send ("OXID\r\n");
          }
       }
    
    void CSCPI::SCPI_Set_Feed (void)
        {
        if (SCPI_ORP_Flag) 
           {
           //if (SCPI_Accumulator < 0.1) pH_Sensor.Set_Reverse_Feed (false);
              //else pH_Sensor.Set_Reverse_Feed (true);
           } 
                     
        if (SCPI_pH_Flag)  
           {
           //if (SCPI_Accumulator < 0.1) pH_Sensor.Set_Reverse_Feed (false);
               //else pH_Sensor.Set_Reverse_Feed (true);
           }             
        }
        
    void CSCPI::SCPI_Put_Mode (void)
       {
       if (SCPI_pH_Flag)
          {
          //switch (pH_Sensor.Get_Mode ( ))
             //{
             //case Off_Mode:  PCBA_CH250K::RS232_Send ("OFF\r\n");  break;
             //case Man_Mode:  PCBA_CH250K::RS232_Send ("MAN\r\n");  break;
             //case Auto_Mode: PCBA_CH250K::RS232_Send ("AUTO\r\n"); break; 
             //}
          }
          
       if (SCPI_ORP_Flag)
          {
          //switch (ORP_Sensor.Get_Mode ( ))
             //{
             //case Off_Mode:  PCBA_CH250K::RS232_Send ("OFF\r\n");  break;
             //case Man_Mode:  PCBA_CH250K::RS232_Send ("MAN\r\n");  break;
             //case Auto_Mode: PCBA_CH250K::RS232_Send ("AUTO\r\n"); break;
             //}    
          } 
        } 
              
     void CSCPI::SCPI_Set_Mode (void)
        {
        int Command;
        
        Command = (int) SCPI_Accumulator;
        
        if (SCPI_ORP_Flag) 
           {
           //switch (Command)
              //{
              //case 0: ORP_Sensor.Set_Mode (Off_Mode);   break;  
              //case 1: ORP_Sensor.Set_Mode (Man_Mode);   break;  
              //case 2: ORP_Sensor.Set_Mode (Auto_Mode);  break;   
              //}
           }
                      
        if (SCPI_pH_Flag) 
           {
           //switch (Command) 
              //{
              //case 0: pH_Sensor.Set_Mode (Off_Mode);  break; 
              //case 1: pH_Sensor.Set_Mode (Man_Mode);  break; 
              //case 2: pH_Sensor.Set_Mode (Auto_Mode); break; 
              //}  
           }                      
        }   
        
        
    void CSCPI::SCPI_Put_ORP (double ORP)
       {
       char str [20];
       
       sprintf (str, "%d\r\n", (int) ORP);
      // PCBA_CH250K::RS232_Send (str);
       }
	   
	  
  void CSCPI::SCPI_Put_PPM (double PPM)
	  {
	  char str [20];
	  
	  sprintf (str, "%d\r\n", (int) PPM);
	//  PCBA_CH250K::RS232_Send (str);
	  }
    
    void CSCPI::SCPI_Put_Flow (void)
       {
       //if (Flow_Armed)
          //{
        //  if (Flow_OK ( ))  PCBA_CH250K::RS232_Send ("OK\r\n");
         //    else PCBA_CH250K::RS232_Send ("NOFLOW\r\n");
          //}
          //else PCBA_CH250K::RS232_Send ("OK\r\n");
       }
    
    void CSCPI::SCPI_Set_Flow (void)
       {
       //if (SCPI_Accumulator < 0.1) Flow_Sensor.Flow_Armed = false;
          //else Flow_Armed = true;
       }
    
    void CSCPI::SCPI_Put_LOLIMIT (void)
       {
       //if (SCPI_ORP_Flag) SCPI_Put_ORP (ORP_Sensor.Get_Lo_Limit ( ));
       //if (SCPI_pH_Flag)  SCPI_Put_pH  (pH_Sensor.Get_Lo_Limit ( ));
       }
    
    void CSCPI::SCPI_Set_LOLIMIT (void)
       {
       //if (SCPI_ORP_Flag) ORP_Sensor.Set_Lo_Limit (SCPI_Accumulator);
       //if (SCPI_pH_Flag)  pH_Sensor.Set_Lo_Limit (SCPI_Accumulator);   
       }
       
    void CSCPI::SCPI_Put_HILIMIT (void)
       {
       //if (SCPI_ORP_Flag) SCPI_Put_ORP (ORP_Sensor.Get_Hi_Limit ( ));
       //if (SCPI_pH_Flag)  SCPI_Put_pH  (pH_Sensor.Get_Hi_Limit ( ));
       }
       
   void CSCPI::SCPI_Set_HILIMIT (void)
      {
      //if (SCPI_ORP_Flag) ORP_Sensor.Set_Hi_Limit (SCPI_Accumulator);
      //if (SCPI_pH_Flag)  pH_Sensor.Set_Hi_Limit (SCPI_Accumulator);
      }
     
   void CSCPI::SCPI_Put_Setpoint (void)
      {
      //if (SCPI_ORP_Flag) SCPI_Put_ORP (ORP_Sensor.Get_Set_Point ( ));
      //if (SCPI_pH_Flag)  SCPI_Put_pH  (pH_Sensor.Get_Set_Point ( ));
      }

   void CSCPI::SCPI_Set_Setpoint (void)
      {
      //if (SCPI_ORP_Flag) ORP_Sensor.Set_Set_Point (SCPI_Accumulator);
      //if (SCPI_pH_Flag)  pH_Sensor.Set_Set_Point  (SCPI_Accumulator);
      }
    
   void CSCPI::SCPI_Put_Proportional (void)
      {
      //if (SCPI_ORP_Flag) SCPI_Put_ORP (ORP_Sensor.Get_Prop_Limit ( ));
      //if (SCPI_pH_Flag)  SCPI_Put_ORP (pH_Sensor.Get_Prop_Limit ( ));
      }
     
   void CSCPI::SCPI_Set_Proportional (void)
     {
     //if (SCPI_ORP_Flag) ORP_Sensor.Set_Prop_Limit (SCPI_Accumulator);
     //if (SCPI_pH_Flag)  pH_Sensor.Set_Prop_Limit  (SCPI_Accumulator);
     }
     
  void CSCPI::SCPI_Put_Safety (void)
      {
      //if (SCPI_ORP_Flag) SCPI_Put_ORP (ORP_Sensor.Get_Safety_Time_Limit ( ));
      //if (SCPI_pH_Flag)  SCPI_Put_pH  (pH_Sensor.Get_Safety_Time_Limit ( ));
      }
      
  void CSCPI::SCPI_Set_Safety (void)
     {
     //if (SCPI_ORP_Flag) ORP_Sensor.Set_Safety_Time_Limit (SCPI_Accumulator);
     //if (SCPI_pH_Flag)  pH_Sensor.Set_Safety_Time_Limit  (SCPI_Accumulator);
     }    
  
  void CSCPI::SCPI_Put_Interlock (void)
     {
     //if (!pH_Interlock_Armed)  PCBA_CH250K::RS232_Send ("OFF\r\n");
        //else
        //{
        //if (pH_Interlock_Tripped)PCBA_CH250K::RS232_Send ("TRIP\r\n"); 
           //else  PCBA_CH250K::RS232_Send ("ARMED\r\n");
        //}
     }
  
  void CSCPI::SCPI_Set_Interlock (void)
     {
     //if (SCPI_Accumulator < 0.1) pH_Interlock_Armed = false;
        //else pH_Interlock_Armed = true;
     }
     
  void CSCPI::SCPI_Put_Status (void)
     {     //IV:144
     int Acc;                 // Status accumulator
     char str [10];
      
      
     
     //Acc =  pH_Sensor.Get_Status ( );
     //Acc += ORP_Sensor.Get_Status ( ) << 4;
     //if (Flow_Sensor.Alarm ( ))            Acc +=   256;
     //if (pH_Interlock_Armed)               Acc +=   512;
     //if (pH_Interlock_Tripped)             Acc +=  1024;
     //if (pH_Sensor.Safety_Timer_Tripped)   Acc +=  2048;
     //if (ORP_Sensor.Safety_Timer_Tripped)  Acc +=  4096;
     //if (pH_Sensor.Prop_Feed_Inhibit ( ))  Acc +=  8192;
     //if (ORP_Sensor.Prop_Feed_Inhibit ( )) Acc += 16384;
      
     sprintf (str, "%d\r\n", (int) Acc);
    // PCBA_CH250K::RS232_Send (str);
  
     //if ( pH_Sensor.)
     //bool Alarm_flag;
     //
     //Alarm_flag = false;   
     //if (pH_Sensor.Alarm ( ))   {PCBA_CH250K::RS232_Send ("pH Alarm ");   Alarm_flag = true;}
     //if (ORP_Sensor.Alarm ( ))  {PCBA_CH250K::RS232_Send ("ORP Alarm ");  Alarm_flag = true;}
     //if (Flow_Sensor.Alarm ( )) {PCBA_CH250K::RS232_Send ("Flow Alarm "); Alarm_flag = true;}   
     //if (!Alarm_flag) 
       //{    
       //PCBA_CH250K::RS232_Send ("OK "); 
       //} 
     //PCBA_CH250K::RS232_Send ("OK\r\n");       
     }
          

  void CSCPI::SCPI_Put_IDN (void)
     {
  //   PCBA_CH250K::RS232_Send ("Chemtrol CH-250, Ver 5.1, Rev 1\r\n");  
     }
 
 //----------------------------------------------------------------------------
 void CSCPI::SCPI_Accumulate_Init (void)
    {
    SCPI_Accumulator = 0.0;
    SCPI_Decimal = false;
    SCPI_Factor = 0.1;
    }
 
 
 bool CSCPI::SCPI_Accumulate (char C)
    {
    bool flag;
    
    flag = false;
    if (!SCPI_Decimal)    
      {
      if ((C >= '0') && (C <= '9'))  
         SCPI_Accumulator = 10.0 * SCPI_Accumulator + (double) C - 48.0;
      if (C == '.') SCPI_Decimal = true;
      } 
      else
      {
      if ((C >= '0') && (C <= '9'))
         {
         SCPI_Accumulator = SCPI_Accumulator + ((double) C - 48) * SCPI_Factor; 
         SCPI_Factor /= 10.0;
         }
      }      
    if (C == ' ')  flag = true;
    if (C == '\n') flag = true; 
    if (C == ':') flag = true;               
    return flag; 
    }
     
 //---------------------------------------------------------------------------
#define ERROR_STATE 997
//#define SPACE_STATE 998
//#define CONT_STATE  999
// C	Next char
// E	Expected Character
// M	Next state for a match
// Q	Next state for a Query
// S	Next state for a Separator
// 

 int CSCPI::SCPI_Eat_Char (char C, char E, int M, int Q, int S)
    {                            // Eats a character from serial port
    if (C == E)    return M;     // A Match?
    switch (C)
       {
       case '?':  return Q;  break;    // A query?
       case ' ':  return S;  break;    // A space?
       case '\r': return S;  break;    // A CR?
       case ':':  return 50; break;    // A : ?  
       }
    return ERROR_STATE;                // Default
    }
   
	
 void CSCPI::SCPI_Process (char *buff)
   {
   int S;
   int NS;
   int i;
   char ch;
   int p;
   char str [40];
   
   S = 0;
   NS = 0;
   p = 0;
   
   //PCBA_CH250K::RS232_Send ("Process -----");      // Debug print
   //PCBA_CH250K::RS232_Send (buff);
   
   //for (i = 0; i < RX_LINE_BUFF_SIZE; i++)
      //{
      //ch = Util::ToUpper (buff [p]);               // Get character
      //p += 1;   
      //if (ch == '\0')  break;                      // Point to next
      //
   //PCBA_CH250K::RS232_Send (ch);                // Debug print
   //sprintf (str, "\r\n Start %d", ch );
   //PCBA_CH250K::RS232_Send (str);
   
      switch (S)                                   // Ref IV:72
         {
         case 0:
            switch (ch)
               {
               case '*': NS = 10; break;
               case 'C': NS = 40; break;
               case ':': NS = 50; break;
               default:  NS = 0;  break;
               } 
            SCPI_Accumulate_Init ( );
            break; 
        //------------* Commands--------------------------------------------------------                 
        case 10:
           switch (ch) 
              {
              case 'I': NS = 11; break;
              // other * commands go here
              default:   NS = ERROR_STATE; break;   
              }
           break;      
        case 11: NS = SCPI_Eat_Char (ch, 'D', 12, ERROR_STATE, ERROR_STATE); break;    
        case 12: NS = SCPI_Eat_Char (ch, 'N', 13, ERROR_STATE, ERROR_STATE); break; 
        case 13:
           switch (ch)                                                              // Fix this (streamline)
              {
              case '?':  SCPI_Put_IDN ( ); NS = 0; break;
              default: NS = ERROR_STATE; break;
              }
           break;
      //------------------------------------------------------------------------------------ 
        case 40: NS = SCPI_Eat_Char (ch, 'H', 41, ERROR_STATE, ERROR_STATE); break;
        case 41: NS = SCPI_Eat_Char (ch, 'E', 42, ERROR_STATE, ERROR_STATE); break;
        case 42: NS = SCPI_Eat_Char (ch, 'M', 43, ERROR_STATE, ERROR_STATE); break;
        case 43: NS = SCPI_Eat_Char (ch, 'T', 44, ERROR_STATE, 48); break;
        case 44: NS = SCPI_Eat_Char (ch, 'R', 45, ERROR_STATE, 48); break;
        case 45: NS = SCPI_Eat_Char (ch, 'O', 46, ERROR_STATE, 48); break;
        case 46: NS = SCPI_Eat_Char (ch, 'L', 47, ERROR_STATE, 48); break;  
       
        case 48: SCPI_Chemtrol_Flag = true;
           if (ch == ':') NS = 50; else NS = 0; break;
      //--------------------------------------------------------------------------------------
       case 50:                        // Got a :
          switch (ch)
             {
             case 'C': NS = 40;  break;    // Chemtrol 
             case 'O': NS = 51;  break;    // orp
             case 'P': NS = 61;  break;    // ph, ph feed, proportional  
             case 'S': NS = 151; break;    // set point, safety, status  
             case 'L': NS = 111; break;    // Lo limit  
             case 'H': NS = 121; break;    // High Limit  
             case 'F': NS = 71;  break;    // Flow 
             case 'I': NS = 211; break;    // Interlock 
             case 'M': NS = 91;  break;    // Mode 
             case 'R': NS = 171;  break;   // Reset
             default:  NS = ERROR_STATE; break;    
            }                
          break;
          
     //--------ORP---------------------------------
     case 51: NS = SCPI_Eat_Char (ch, 'R', 52, ERROR_STATE, ERROR_STATE); break;   
     case 52: NS = SCPI_Eat_Char (ch, 'P', 53, ERROR_STATE, ERROR_STATE); break;      
     case 53: NS = SCPI_Eat_Char (ch, '?', 54, 54, 0); SCPI_ORP_Flag = true; SCPI_pH_Flag = false; break;
     case 54: NS = 0; //SCPI_Put_ORP (ORP_Sensor.Current ( )); break;
    
     //---------pH---------------------------------------------------------------
     case 61:
        switch (ch)
           {
           case 'H':
              SCPI_pH_Flag  = true;
              SCPI_ORP_Flag = false;
              NS = 62;  
              break;
           case 'R': NS = 131; break;
           default:   NS = ERROR_STATE; break;  
           }
        break; 
        
     case 62:   
        NS = SCPI_Eat_Char (ch, '?', 63, 63, ERROR_STATE); break;                    
        SCPI_pH_Flag  = true; 
        SCPI_ORP_Flag = false;
        break; 
        
     case 63: 
        //SCPI_Put_pH (pH_Sensor.Current ( ));
        if (ch == ':') NS = 50;
           else NS = 0;
        break;

     //     
     //------------Flow----------------------------------------------------------
     //case 71: NS = SCPI_Eat_Char (ch, 'L', 72, ERROR_STATE, ERROR_STATE); break;
     case 71: 
        switch (ch)
           {
           case 'L': NS = 72; break;
           case 'E': NS = 82; break;
           default: NS = ERROR_STATE; break;
           }
        break;
     case 72: NS = SCPI_Eat_Char (ch, 'O', 73, ERROR_STATE, ERROR_STATE); break;
     case 73: NS = SCPI_Eat_Char (ch, 'W', 74, ERROR_STATE, ERROR_STATE); break;
     case 74: NS = SCPI_Eat_Char (ch, '?', 78, 78, 79); break;
     case 78: 
        SCPI_Put_Flow ( ); 
        if (ch == ':') NS = 50; else NS = 0;
        break;
     case 76:
         NS = 76;
         if (SCPI_Accumulate (ch))
            {
            SCPI_Set_Flow ( );
            if (ch == ':') NS = 50; else NS = 0;
            }
         break;
           
     //--------Feed-------------------------------------------------------
     
     case 82: NS = SCPI_Eat_Char (ch, 'E', 83, ERROR_STATE, ERROR_STATE); break;
     case 83: NS = SCPI_Eat_Char (ch, 'D', 84, ERROR_STATE, ERROR_STATE); break; // 70 here
     case 84: NS = SCPI_Eat_Char (ch, '?', 88, 88, 89); break;
     case 88:
        SCPI_Put_Feed ( ); //
        if (ch == ':') NS = 50; else NS = 0;
        break;
     case 89: 
        NS = 89;
        if (SCPI_Accumulate (ch))
           {
           SCPI_Set_Feed ( );
           if (ch == ':') NS = 50; else NS = 0;
           }
        break;
        
      //-----Mode -----------------------------------------------------
      case 91: NS = SCPI_Eat_Char (ch, 'O', 92, ERROR_STATE, ERROR_STATE); break;
      case 92: NS = SCPI_Eat_Char (ch, 'D', 93, ERROR_STATE, ERROR_STATE); break;
      case 93: NS = SCPI_Eat_Char (ch, 'E', 94, ERROR_STATE, ERROR_STATE); break;
      case 94: NS = SCPI_Eat_Char (ch, '?', 98, 98, 99); break;
      case 98:
         NS = 98;
         SCPI_Put_Mode ( );
         if (ch == ':') NS = 50; else NS = 0;
         break;
      case 99:
         NS = 99;
         if (SCPI_Accumulate (ch))
            {
            SCPI_Set_Mode ( );
            //Mode_Update_Flag = true;
            if (ch == ':') NS = 50; else NS = 0;
            }
         break;
              
     // ---------- Low limit----------------------------------------------     
     case 111: NS = SCPI_Eat_Char (ch, 'O', 112, ERROR_STATE, ERROR_STATE); break;
     case 112: NS = SCPI_Eat_Char (ch, 'L', 113, ERROR_STATE, ERROR_STATE); break;
     case 113: NS = SCPI_Eat_Char (ch, 'I', 114, ERROR_STATE, ERROR_STATE); break;
     case 114: NS = SCPI_Eat_Char (ch, 'M', 115, ERROR_STATE, ERROR_STATE); break;
     case 115: NS = SCPI_Eat_Char (ch, 'I', 116, 118, 119); break;
     case 116: NS = SCPI_Eat_Char (ch, 'T', 117, 118, 119); break; 
     case 117: NS = SCPI_Eat_Char (ch, '?', 118, 118, 119); break;
     case 118:                     
         SCPI_Put_LOLIMIT ( ); 
         if (ch == ':') NS = 50; else NS = 0; 
         break;
     case 119: 
         NS = 119; 
         if (SCPI_Accumulate (ch)) 
            {
            SCPI_Set_LOLIMIT ( );
            if (ch == ':') NS = 50; else NS = 0;
            }               
        break;                   // get number
    
   //---------------High Limit---------------------------------------------------
     case 121: NS = SCPI_Eat_Char (ch, 'I', 122, ERROR_STATE, ERROR_STATE); break;
     case 122: NS = SCPI_Eat_Char (ch, 'L', 123, ERROR_STATE, ERROR_STATE); break;
     case 123: NS = SCPI_Eat_Char (ch, 'I', 124, ERROR_STATE, ERROR_STATE); break;
     case 124: NS = SCPI_Eat_Char (ch, 'M', 125, ERROR_STATE, ERROR_STATE); break;
     case 125: NS = SCPI_Eat_Char (ch, 'I', 126, 128, 129); break;
     case 126: NS = SCPI_Eat_Char (ch, 'T', 127, 128, 129); break;
     case 127: NS = SCPI_Eat_Char (ch, '?', 128, 128, 129); break;
     case 128:
        SCPI_Put_HILIMIT ( ); //         NS = SCPI_Eat_Char (ch, '?', 119, 119, 0)
        if (ch == ':') NS = 50; else NS = 0; 
        break;
     case 129: 
        NS = 129;
        if (SCPI_Accumulate (ch))
           {
           SCPI_Set_HILIMIT ( );
           if (ch == ':') NS = 50; else NS = 0;
           }
        break;
        
    //----------Proportional---------------------------------------------------
    case 131: NS = SCPI_Eat_Char (ch, 'O', 132, ERROR_STATE, ERROR_STATE); break;
    case 132: NS = SCPI_Eat_Char (ch, 'P', 133, ERROR_STATE, ERROR_STATE); break;
    case 133: NS = SCPI_Eat_Char (ch, 'O', 134, 148, 147); break;
    case 134: NS = SCPI_Eat_Char (ch, 'R', 135, 148, 147); break;
    case 135: NS = SCPI_Eat_Char (ch, 'T', 136, 148, 147); break;
    case 136: NS = SCPI_Eat_Char (ch, 'I', 137, 148, 147); break;
    case 137: NS = SCPI_Eat_Char (ch, 'O', 138, 148, 147); break;
    case 138: NS = SCPI_Eat_Char (ch, 'N', 139, 148, 147); break;
    case 139: NS = SCPI_Eat_Char (ch, 'A', 140, 148, 147); break;
    case 140: NS = SCPI_Eat_Char (ch, 'L', 141, 148, 147); break;
    case 141: NS = SCPI_Eat_Char (ch, '?', 148, 148, 147); break;
    case 148:
       SCPI_Put_Proportional ( );
       if (ch == ':') NS = 50; else NS = 0;
       break;
    case 147:
       NS = 147;
       if (SCPI_Accumulate (ch))
          {
          SCPI_Set_Proportional ( );
          if (ch == ':') NS = 50; else NS = 0;
          }
       break;
    //--------------------------------------------------------------------------
    // -------case 'S': NS = 150; break;    // set point, safety timer, status  
     case 151:
         switch (ch)
            {
            case 'T': NS = 152; break;
            case 'E': NS = 161; break;
            case 'A': NS = 181; break;
            default:  NS = ERROR_STATE; break;
            }
         break;
         
     case 152: NS = SCPI_Eat_Char (ch, 'A', 153, ERROR_STATE, ERROR_STATE); break;
     case 153: NS = SCPI_Eat_Char (ch, 'T', 154, ERROR_STATE, ERROR_STATE); break;
     case 154: NS = SCPI_Eat_Char (ch, 'U', 155, 157, ERROR_STATE); break;
     case 155: NS = SCPI_Eat_Char (ch, 'S', 156, 157, ERROR_STATE); break;    
     case 156: NS = SCPI_Eat_Char (ch, '?', 157, 157, ERROR_STATE); break;
     case 157: 
        SCPI_Put_Status ( );
        if (ch == ':') NS = 50; else NS = 0;
        break;
     
     //-----------Set point-------------------------------------------------------------
     case 161: NS = SCPI_Eat_Char (ch, 'T', 162, ERROR_STATE, ERROR_STATE); break;
     case 162: NS = SCPI_Eat_Char (ch, 'P', 163, ERROR_STATE, ERROR_STATE); break;
     case 163: NS = SCPI_Eat_Char (ch, 'O', 164, 168, 169); break;
     case 164: NS = SCPI_Eat_Char (ch, 'I', 165, 168, 169); break;
     case 165: NS = SCPI_Eat_Char (ch, 'N', 166, 168, 169); break;
     case 166: NS = SCPI_Eat_Char (ch, 'T', 167, 168, 169); break;
     case 167: NS = SCPI_Eat_Char (ch, '?', 168, 168, 169); break;
     case 168:
        SCPI_Put_Setpoint ( );
        if (ch == ':') NS = 50; else NS = 0;
        break;
     case 169:
       NS = 169;
       if (SCPI_Accumulate (ch))
          {
          SCPI_Set_Setpoint ( );
          if (ch == ':') NS = 50; else NS = 0;
          }
       break;
       
       //------- Reset----------------------------------------------------------
       case 171: NS = SCPI_Eat_Char (ch, 'E', 172, ERROR_STATE, ERROR_STATE); break;
       case 172: NS = SCPI_Eat_Char (ch, 'S', 173, ERROR_STATE, ERROR_STATE); break;
       case 173: NS = SCPI_Eat_Char (ch, 'E', 174, ERROR_STATE, ERROR_STATE); break;
       case 174: NS = SCPI_Eat_Char (ch, 'T', 175, ERROR_STATE, ERROR_STATE); break;
       case 175: NS = SCPI_Eat_Char (ch, '!', 178, ERROR_STATE, ERROR_STATE); break;
       case 178:
    //      ORP_Sensor.Set_Default_Parameters ( );
    //      pH_Sensor.Set_Default_Parameters ( );
    //      Flow_Sensor.Set_Default_Parameters ( );
          if (ch == ':') NS = 50; else NS = 0;
          break;


     //--------safety----------------------------------------------------------
     case 181: NS = SCPI_Eat_Char (ch, 'F', 182, ERROR_STATE, ERROR_STATE); break;
     case 182: NS = SCPI_Eat_Char (ch, 'E', 183, ERROR_STATE, ERROR_STATE); break;
     case 183: NS = SCPI_Eat_Char (ch, 'T', 184, 188, 189); break;
     case 184: NS = SCPI_Eat_Char (ch, 'Y', 185, 188, 189); break;
     case 185: NS = SCPI_Eat_Char (ch, '?', 188, 188, 189); break;
     case 188:
        SCPI_Put_Safety ( );
        if (ch == ':') NS = 50; else NS = 0;
           break;
     case 189:
        NS = 189;
        if (SCPI_Accumulate (ch))
           {
           SCPI_Set_Safety ( );
           if (ch == ':') NS = 50; else NS = 0;
           }
        break;
        
    //------- Interlock----------------------------------------------------------
    case 211: NS = SCPI_Eat_Char (ch, 'N', 212, ERROR_STATE, ERROR_STATE); break;
    case 212: NS = SCPI_Eat_Char (ch, 'T', 213, ERROR_STATE, ERROR_STATE); break;
    case 213: NS = SCPI_Eat_Char (ch, 'E', 214, ERROR_STATE, ERROR_STATE); break;
    case 214: NS = SCPI_Eat_Char (ch, 'R', 215, 221, 222); break;
    case 215: NS = SCPI_Eat_Char (ch, 'L', 216, 221, 222); break;
    case 216: NS = SCPI_Eat_Char (ch, 'O', 217, 221, 222); break;
    case 217: NS = SCPI_Eat_Char (ch, 'C', 218, 221, 222); break;
    case 218: NS = SCPI_Eat_Char (ch, 'K', 219, 221, 222); break;
    case 219: NS = SCPI_Eat_Char (ch, '?', 221, 221, 222); break;
    case 221:
       SCPI_Put_Interlock ( );
       if (ch == ':') NS = 50; else NS = 0;
       break;
    case 222:
        NS = 222;
        if (SCPI_Accumulate (ch))
           {
           SCPI_Set_Interlock ( );
           if (ch == ':') NS = 50; else NS = 0;
           }
        break;
        
        }  // End big switch  
        
     if (NS == 0) //break;
     S = NS;
    } // end for i

 
   //} // End SCPI Process
   
           