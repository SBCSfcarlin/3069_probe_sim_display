/*
 * PCBA_SIM18.cpp
 *
 * Created: 7/10/2018 11:38:00
 *  Author: Fredc
 */ 
 /*
  * PCBA201705.cpp
  *
  * Created: 5/25/2017 15:11:31
  *  Author: Fredc
  */ 
   #include <stdio.h>

  #include <avr/io.h>
  #include <avr/interrupt.h>
  #define F_CPU 8000000
  #include <util/delay.h>
  #include <avr/wdt.h>
  //#include "Config.h"

  //#include "util.h"
  #include "PCBA_SIM18.h"

  #define PUD_BIT   0x40       // Pull up disable

  static int  OneSecondTimer;        // 5ms down counter for tracking 1 second intervals
  //static bool OneSecondFlag;       // 1 = 10 sec over
  //static int  HalfSecondTimer;     // 5ms down counter for tracking 1 second intervals
  //static bool HalfSecondFlag;      // 1 = 10 sec over
  static volatile bool Timer_Flag;            // 1 second flag
  static volatile bool Timer_Tick;            // 2 ms Tick
  
  
   
  //---------------------------------------------------
 
  //----------------------
 

  #define PUD_BIT   0x40       // Pull up disable
  
  void PCBA2018::IO_Init ( )
     {
     //DDRA = 0x80; 
     DDRA = 0x03;       //
     PORTA = 0x2f;      // Set pullups and rows hi
     DDRD |= 0xf0;      // Test ports 
     //PORTD &= ~0x70;                
     }
  //     PORTA    PORTB    PORTC    PORTD       Ref VII:14 and VI:136-137
  // 0            Col6     Col4
  // 1                     Col3 
  // 2                     Row2
  // 3                     Row1
  // 4                              Rho Test
  // 5                              PWM out
  // 6                              CTR out
  // 7                              Loop test

  void PCBA2018::Test (int t)
    {            // Testing rho out on PortD 0x80
     // Stub to inset test code
     if (t == 0) PORTD &= ~0x80;
     else PORTD |= 0x80;
    }
  
  //===================Buttons=====================================
  //Ref VI:137
  // Note pin numbers have been reversed. Pin 2 & 4 are open
  //
  #define col3 0x04
  #define col4 0x08
  #define col6 0x20
               int row1 = 0xff;
               int row2 = 0xff;
  Button Buttons::Read_Buttons ( )
     {
     //int row1;
     //int row2;

     PORTA &= ~1;
     _delay_us (100); //
     row1 = PINA;
     PORTA |= 3;

     PORTA &= ~2;
     _delay_us (100); //
     row2 = PINA;
     PORTA |= 3;

     if ((row1 & col3) == 0) return RIGHT;   //Button.
     if ((row2 & col3) == 0) return DOWN;    //Button.
     if ((row1 & col4) == 0) return UP;      //Button.
     if ((row2 & col4) == 0) return LEFT;    //Button.
     if ((row1 & col6) == 0) return OK;      //Button.
     if ((row2 & col6) == 0) return MYSTERY; //Button.
     return NONE;                            //Button.MYSTERY;
     }
 
   
//-----------------------------------Watch Dog----------------------------------------------
//
//Class Watch_Dog
   void Watch_Dog::Init (void)
      {
  	   //Watch_Dog::
      Off ( );       // Turn off WDT
		return;
       
	 //  MCUSR = 0;        // Tiny p46
	   //WDTCR |= 0x69;
	   //wdt_enable (15);
	   //wdt_enable (WDTO_8S);
	   }


void Watch_Dog::Touch (void)
   {
   // WDTCR |= 0x39;
   wdt_reset ( );
   //WDTCR |= 0x1f;
   }

void Watch_Dog::Off  (void)
   {
   //wdt_disable ( );
   //WDTCR |= (1 << WDCE) | (1 << WDE);     // Enable turn off
   //WDTCR = 0x00;                         // Turn off WDT
   
  // MCUSR &= 0x08; //_WDR();/* Clear WDRF in MCUSR */ 
 //  MCUSR = 0x00;            /* Write logical one to WDCE and WDE */
   //WDTCR |= (1<<WDCE) | (1<<WDE);/* Turn off WDT */
  // WDTCR = 0x00;
   }   

void Watch_Dog::Reboot  (void)
   {
   ((void (*)(void))0)();  
   }

//ISR (WDT_vect)
//  {
 // PCBA201705::WatchDog_Off ( );
//  PCBA201705::Reboot ( );
 // }  

 //=========================================Timer============================

 bool PCBA2018::Get_One_Second_Flag (void)
    {
    if (Timer_Flag)           // Flag is up
       {
       Timer_Flag = false;    // Lower
       return true;           //
       }
    return false;
    }

 bool PCBA2018::Get_Timer_Tick (void)
 {
    if (Timer_Tick)
    {
       Timer_Tick = false;
       return true;
    }
    return false;
 }
 

 #define ONESECONDTIME       500    /* Number of ticks for 1 sec    */
// #define HALFSECONDTIME      250    /* Number of ticks for 1/2 sec  */
 //#define TIMER0RELOAD        193    /* for 1 ms timer overflow 193*/
 #define TIMER0RELOAD        241    /* for 2 ms timer overflow 193*/
 
 void PCBA2018::Timer_Init (void)
    {
    //-------Initialize Timer-------------------------------
    cli();
    TCCR0A = 0x00;
    TCCR0B = 0x05;               // 8 bit, Prescaler = 1024 
    TIMSK0 |= 1;                 // TOIE  enable timer overflow interrupt
     
    TCNT0 = TIMER0RELOAD;
    Timer_Flag = false;
    Timer_Tick = false;
    OneSecondTimer = ONESECONDTIME;          // set 200*5ms for the second timer
    //HalfSecondTimer = HALFSECONDTIME;
    }

 //-----------------------------------------------------------------------------------
 ISR (TIMER0_OVF_vect)                       // Overflow detect
    {
    
    TCNT0 = TIMER0RELOAD;

    Timer_Tick = true;                       //
    OneSecondTimer--;                        // Decrement the one second timer
    if (OneSecondTimer == 0)                 // If the second timer has run out ...
       {
       OneSecondTimer = ONESECONDTIME;       // Reload the one second timer
       Timer_Flag = true;                    // Show it has been one second for the check
       }
    if (OneSecondTimer < 0) OneSecondTimer = ONESECONDTIME;
    }
	

 //=============================UARTS=======================================

   void Serial_Port::Init (int port, int baud_rate, int buffer_size)
      {
      hdw_port = port;
      Rx_Buff.CBox_Init  (buffer_size);         // Set up buffers
      Tx_Buff.CBox_Init  (buffer_size);         //
	   UART_init (port, baud_rate);
      }

   void Serial_Port::UART_init (int Port, int baud_rate)
      {
      int UBRR;
   
      UBRR = 103;                            // Default 9600 (8MHz U2X = 1)
      switch (baud_rate)
         {
         case 2400:   UBRR = 416; break;        // Need to set these up for UART0
         case 4800:   UBRR = 207; break;        // Not the same as UART1 :(
         case 9600:   UBRR = 103; break;
         case 14400:  UBRR =  68; break;
         case 19200:  UBRR =  51; break;
         case 28800:  UBRR =  34; break;
        // case 115200: UBRR =   8; break;
         }

      switch (Port)
         {
         case 0:
           UBRR0H = UBRR >> 8;     // Set baud rate
           UBRR0L = UBRR;          //
           UCSR0A = 2;             // U2X set to 1 ( doubling)
           UCSR0C = 0x06;          // Set frame format: 8N1
           UCSR0B = 0x18;          // Enable receiver and transmitter
           break;

        case 1:
          UBRR1H = UBRR >> 8;     // Set baud rate
          UBRR1L = UBRR;          //
          UCSR1A = 2;             // U2X set to 1 ( doubling)
          UCSR1C = 0x06;          // Set frame format: 8N1
          UCSR1B = 0x18;          // Enable receiver and transmitter
          break;
         }

      }
   
   
  void Serial_Port::Tx_Check ( )        // Update 
      {  
      if (Tx_Buff.Ready ( ))              // Character available?
         {                                //
         switch (hdw_port)                // If so, ship it
            {
            case 0:
               if ((UCSR0A & 0x20) != 0) UDR0 = Tx_Buff.Get_Char  ( );   
               break;
            case 1:
               if ((UCSR1A & 0x20) != 0) UDR1 = Tx_Buff.Get_Char  ( );   
               break;
            }    
         }  
      }


  void Serial_Port::Rx_Check ( )           // Character available?
        {
        //char next;

        switch (hdw_port)                   // If so, suck it up
           {
           case 0:
              if ((UCSR0A & 0x80) != 0) Rx_Buff.Put_Char (UDR0);
              break;
           case 1:
              if ((UCSR1A & 0x80) != 0) Rx_Buff.Put_Char (UDR1);
              break;
          }
        }
   

  void Serial_Port::Send (char ch)     // Send a char
      {
      Tx_Buff.Put_Char  (ch);       
      } 

  void Serial_Port::Send (char *ptr)    // Send a line
      {
      while  (*ptr != 0)
         {
         Tx_Buff.Put_Char (*ptr++);  
         }
      }


  char Serial_Port::Get_Char ( )       // Returns next char or null  
     {
     if (Rx_Buff.Ready ( ))            // Character waiting?
        return Rx_Buff.Get_Char ( );
     return '\0';
     }
      

//

////======================Analog_Converter ========================================
   void Analog_Converter::Init  ( )
   {
      //ADCSRA = 0;
    ////  PRR = 0;                         // Power Reduction p DK V:45, VI:180
    ////  ADCSRB = 0;                      // Disable Analog Compare mode  (p 265)
   }
   ////---------------------ADC-----------------------------
   //#define ADPS 4                         // ADC Pre-scaler  (p 160)
   //// Division Factors  (0..7)                                        // 2 2 4 8 16 32 64 128
   void Analog_Converter::Start  (int chan, bool High_Range)
      {
      ////while  ( (ADCSRA & 0x40) != 0)     // Wait till done
      ////;
      ////if  (High_Range)
      ////{
         ////ADMUX = chan & 0x07;             // Remote
        ////// ADCSRB = 0;
      ////}
      ////else
      ////{
         ////ADMUX =  (chan & 0x07) | 0x80;
        ////// ADCSRB |= 0x10;
      ////}
      ////ADCSRA = ADPS;                   // Select Channel
      ////ADCSRA = 0xc0 | ADPS;            // Start Conversion
      }
//
   bool Analog_Converter::Done  ( )
   {
      //if  ( (ADCSRA & 0x40) != 0)    // Check SC Bit
      ////if  (! (ADCSRA & 0x10))      // Check IF Bit
      //return false;
//
      ////ADCSRA |= 0x10;            // Clear IF Bit
      return true;
   }
//
   int Analog_Converter::Read  ( )
      {
      int reading;
   //
      reading = 0;
      //while  ( (ADCSRA & 0x40) != 0)              // Wait till done
      //;
      //reading = ADCL & 0xff;                 // Must read L first
      //reading =  (ADCH << 8) | reading;       // Then H  (hardware requirement)
      //reading &= 0x3ff;
      //// ADCSRA = ADPS;                       // ADC off
      return reading;
      }
//

   //
   //=======================PWM==================================
   // class Pulse_Wave_Modulator
   // Ref II:170 VII:27
   void Pulse_Wave_Modulator::Init  ( )
      {
      //PLLCSR = 0x02;         // Enable PLL
      //_delay_ms  (150);       // Wait a while
      //while  ( (PLLCSR & 1) == 0) ;     // Wait for lock
      //
      //PLLCSR = 0x06;         // PLL 84 low

      TCCR1A = 0xc3; // 0xc2;         // 21  VII:33 (43)s
      TCCR1B = 0x0b; //0x11;            //1         (11)
      //TCCR1C = 0x30;         // Shadow
      //TCCR1D = 1;
      //TCCR1E = 0;            // Output compare override //4
      
      
      //OCR1AH = 0x07;         // Set top fff
     
      //OCR1AH = 0x0f;         // Set top fff
      //OCR1AL = 0xff;

      //ICR1L  = 0xff;
      //ICR1H  = 0x0f;         // 
      //ICR1L  = 0xff;

      //TCNT1H  = 0x00;
      //TCNT1L  = 0x00;
      }
//
//
 void  Pulse_Wave_Modulator::Set  (int pwm)
    {
    int cnt;

    cnt = 0x3ff -  (pwm & 0x3ff);
    //cnt = pwm;  
    OCR1AH =  (cnt >> 8) & 0x03; 
    OCR1AL = cnt & 0xff;
   

    ////ICR1L = cnt & 0xff;
   // ICR1H =  (cnt >> 8) & 0x0f;
   // ICR1L = cnt & 0xff;
   
   
    }
 
 //void PCBA201705::PWM_Set (double volts)
 //{
 //int    counts;
 //double countsf;
 //
 ////countsf = 102.3 * volts;       // 10.0 v is full scale
 ////countsf = 204.6 * volts;         // 5.0v is full scale
 //countsf = 409.6 * volts;         // 2.5v is full scale
 //counts = Util::Bracket ((int) countsf, 1023, 0);
 //PWM_Set (counts);
 //}
  //============================================================

 //=================Rho Outputs===================================
 static volatile int RhoA_Count;
 //static volatile int RhoB_Count;
 static volatile double RhoA_Freq;
 //static volatile double RhoB_Freq;
 static volatile bool RhoA_Out;
 //static volatile bool RhoB_Out;

 void Rho_Output::Init (void)
    {
    RhoA_Count = 500;
    //RhoB_Count = 500;
    RhoA_Freq = 1.0;
    //RhoB_Freq = 1.0;
    RhoA_Out = false;
    // RhoB_Out = false;

    //DDRD |= 0x40;        // Set PD6 as output //see line 48
                           // THis is OC2B
    TCCR2A = 0x12;         // Set toggle mode
    TCCR2B = 0x05;         // Set toggle prescale = 5
    OCR2B  = 0x7f;         // Compare
    OCR2A  = 0x7f;

    }

 void Rho_Output::Test (bool level)        // Strobe Rho
    {
    //RhoA_Count -= 1;
    //if (RhoA_Count == 0)
    //{
    
    RhoA (level);          // This sets the output

    //if (RhoA_Out) PORTD |= 0x10;    // Pulse checker
    //   else PORTD &= ~0x10;
    //RhoA_Count = (int) (250.0 / RhoA_Freq);
    //}

    //RhoB_Count -= 1;
    //if (RhoB_Count == 0)
    //{
    //RhoB_Out = !RhoB_Out;
    //RhoB (RhoB_Out);
    //RhoB_Count = (int) (250.0 / RhoB_Freq);
    //}
    }

 void Rho_Output::Set_FreqA (double freq)
    {
    RhoA_Freq = freq;
    }
 //void Rho_Output::Set_FreqB (double freq)
 //{
 //RhoB_Freq = freq;
 //}

 double Rho_Output::Get_FreqA (void)
    {
    return RhoA_Freq;
    }

 //double Rho_Output::Get_FreqB (void)
 //{
 //return RhoB_Freq;
 //}
 //----------------------------------
 void Rho_Output::RhoA (bool A)
    {
    if (A) PORTD |= 0x10;
    else PORTD &= ~0x10;
    }
 //void Rho_Output::RhoB (bool B)
 //{
 //if (B) PORTD |= 0x40;
 //else PORTD &= ~0x40;
 //}
 
