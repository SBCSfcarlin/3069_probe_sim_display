/*
 * EEProm.cpp
 *
 * Created: 8/1/2017 13:32:29
 *  Author: Fredc
 */ 
/*
 * EEProm.cpp
 *
 * Created: 11/21/2013 12:31:37 
 *  Author: fcarlin
 */ 
 /*******************************************************************************
 *
 * EEProm.c
 *
 * Contains routines that deal with EEProm
 *
 *******************************************************************************/

//

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/common.h> 

////
 ////#include "config.h"
 #include "EEProm.h"
////
 //////---------------------Lay out of EEROM---------------------------
  ////Define addresses
 ////#define EEPROM_BASE	               0x10	   // For some reason TT doesn't like writing in first row  
 ////#define EEINITCOMPLETE             0x10     // This is 0xFF after a chip erase
 ////
                                       //////    and is set to 0x00 when all other EEPROM values
                                       //////    have been set to their default values
 ////#define EEBAUD_RATE                0x12     // Not indexed by switch                            
 ////#define EESERIALNUMBER             0x14     // SERIAL NUMBER for this unit 
 ////#define EECONTROLLERID             0x20     // EEPROM address for controller's ID (0)  
 ////#define apH_Set                    0x24     // pH Set point
 ////#define aORP_Set                   0x28     // ORP Set point
 ////#define aFlo_Set                   0x2c     // Flow Set point
 ////#define apH_HiLim                  0x30     // pH Hi Limit
 ////#define aORP_HiLim                 0x34     // ORP Hi Limit
 ////#define aFlo_HiLim                 0x38     // Flo Hi Limit 
 ////#define apH_LoLim                  0x3c     // pH Lo Limit
 ////#define aORP_LoLim                 0x40     // ORP Lo Limit         
 ////#define aFlo_LoLim                 0x44     // Flo Lo Limit   
 ////#define aORP_Safety                0x48     // ORP Safety time
 ////#define apH_Safety                 0x4c     // pH Safety Time    
 ////#define apH_Prop                   0x50     // pH Safety Time    
 ////#define aORP_Prop                  0x54     // pH Safety Time                    //
 ////#define apH_Cal                    0x58     // pH Calibration  
 ////#define aORP_RM                    0x5c     // ORP Run Mode 
 ////#define apH_RM                     0x60     // pH Run Mode
////
//////==============================================================================================
////// Compute the base address of the area used for this 
////
//////-----------Baud Rate-----------------------------------------
//////
////
////void EEProm::Save_pH_SetPoint (double pH_Set)        {WriteDoubleEE (apH_Set, pH_Set); }   
////void EEProm::Save_pH_Hi_Limit (double pH_HiLim)      {WriteDoubleEE (apH_HiLim, pH_HiLim); }   
////void EEProm::Save_pH_Lo_Limit (double pH_LoLim)      {WriteDoubleEE (apH_LoLim, pH_LoLim); }  
////void EEProm::Save_pH_Safety_Limit (double pH_Safety) {WriteDoubleEE (apH_Safety, pH_Safety); }
////void EEProm::Save_pH_Prop_Limit (double pH_Prop)     {WriteDoubleEE (apH_Prop, pH_Prop); }
////void EEProm::Save_pH_Cal_Target (double pH_Cal)      {WriteDoubleEE (apH_Cal, pH_Cal); }
    ////
////void EEProm::Save_ORP_SetPoint (double ORP_Set)        {WriteDoubleEE (aORP_Set, ORP_Set); }
////void EEProm::Save_ORP_Hi_Limit (double ORP_HiLim)      {WriteDoubleEE (aORP_HiLim, ORP_HiLim); }   
////void EEProm::Save_ORP_Lo_Limit (double ORP_LoLim)      {WriteDoubleEE (aORP_LoLim, ORP_LoLim); }
////void EEProm::Save_ORP_Safety_Limit (double ORP_Safety) {WriteDoubleEE (aORP_Safety, ORP_Safety); }
////void EEProm::Save_ORP_Prop_Limit (double ORP_Prop)     {WriteDoubleEE (aORP_Prop, ORP_Prop); }   
    ////
////void EEProm::Save_Flow_SetPoint (double Flow_Set)  {WriteDoubleEE (aFlo_Set, Flow_Set); }
////void EEProm::Save_Flow_Hi_Limit (double Flo_HiLim) {WriteDoubleEE (aFlo_HiLim, Flo_HiLim); }   
////void EEProm::Save_Flow_Lo_Limit (double Flo_LoLim) {WriteDoubleEE (aFlo_LoLim, Flo_LoLim); }
////
////double EEProm::Get_pH_SetPoint (void)     {return ReadDoubleEE (apH_Set); }     
////double EEProm::Get_pH_Hi_Limit (void)     {return ReadDoubleEE (apH_HiLim); }           
////double EEProm::Get_pH_Lo_Limit (void)     {return ReadDoubleEE (apH_LoLim); }
////double EEProm::Get_pH_Safety_Limit (void) {return ReadDoubleEE (apH_Safety);}
////double EEProm::Get_pH_Prop_Limit (void)   {return ReadDoubleEE (apH_Prop);}  
////double EEProm::Get_pH_Cal_Target (void)   {return ReadDoubleEE (apH_Cal);}  
   ////
////double EEProm::Get_ORP_SetPoint (void)     {return ReadDoubleEE (aORP_Set); }  
////double EEProm::Get_ORP_Hi_Limit (void)     {return ReadDoubleEE (aORP_HiLim); }    
////double EEProm::Get_ORP_Lo_Limit (void)     {return ReadDoubleEE (aORP_LoLim); }    
////double EEProm::Get_ORP_Safety_Limit (void) {return ReadDoubleEE (aORP_Safety);}
////double EEProm::Get_ORP_Prop_Limit (void)   {return ReadDoubleEE (aORP_Prop);}
   ////
////double EEProm::Get_Flow_SetPoint (void)   {return ReadDoubleEE (aFlo_Set); } 
////double EEProm::Get_Flow_Lo_Limit (void)   {return ReadDoubleEE (aFlo_LoLim); }  
////double EEProm::Get_Flow_Hi_Limit (void)   {return ReadDoubleEE (aFlo_HiLim); }   
 ////
////void EEProm::Save_ORP_Mode (Run_Mode RM) {WriteWordEE (aORP_RM, (unsigned char) RM);}
////Run_Mode EEProm::Get_ORP_Mode  (void)        {return (Run_Mode) ReadWordEE (aORP_RM);}
////void EEProm::Save_pH_Mode  (Run_Mode RM) {WriteWordEE (apH_RM,  (unsigned char) RM);}
////Run_Mode EEProm::Get_pH_Mode   (void)        {return (Run_Mode) ReadWordEE (apH_RM);} 
//////-------------------------------------------------------------------
//////
////
void EEProm::Init (void)
   {
   ////unsigned int temp;

   ////temp = ReadWordEE (EEINITCOMPLETE);
   ////if (temp != 0x1234)                         // if EEPROM has not been initialized  
      ////{
      ////EEProm_Set_Default ( ); 
      ////WriteWordEE (EEINITCOMPLETE, 0x1234);
      ////}
   }
////
////
////
////void EEProm::EEProm_Set_Default (void)
   ////{
   ////Save_pH_SetPoint (pH_DEFAULT_SETPOINT);
   ////Save_pH_Hi_Limit (pH_DEFAULT_HI_LIMIT);
   ////Save_pH_Lo_Limit (pH_DEFAULT_LO_LIMIT);
   ////Save_pH_Safety_Limit (pH_DEFAULT_SAFETY_TIME);
   ////Save_pH_Prop_Limit (pH_DEFAULT_PROP_LIMIT);
   ////Save_pH_Cal_Target (pH_DEFAULT_CAL_TARGET);
   ////
   ////Save_ORP_SetPoint (ORP_DEFAULT_SETPOINT);
   ////Save_ORP_Hi_Limit (ORP_DEFAULT_HI_LIMIT);
   ////Save_ORP_Lo_Limit (ORP_DEFAULT_LO_LIMIT);  
   ////Save_ORP_Safety_Limit (ORP_DEFAULT_SAFETY_TIME);
   ////Save_ORP_Prop_Limit (ORP_DEFAULT_PROP_LIMIT);
  ////
   ////Save_Flow_SetPoint (Flow_DEFAULT_SETPOINT);
   ////Save_Flow_Hi_Limit (Flow_DEFAULT_HI_LIMIT);
   ////Save_Flow_Lo_Limit (Flow_DEFAULT_LO_LIMIT);
   ////
   ////}
////
   //////===========EEProm Stuff===============================================================
   //////
     ////secure char read/write to EEPROM (interrupts are disabled during write)
////
////void EEProm::WriteByteEE (int address, unsigned char data)
   ////{
   ////unsigned char temp;                 // local temp variable
      ////
   ////#if !defined(_MSC_VER)
   ////while (EECR & 0x02){}               // wait for EEPROM to be ready
   ////#endif
   ////temp = SREG;                        // store status register 
   ////cli ();									   // disable interrupts
   ////EEAR = address;                     // set EEPROM address
   ////EEDR = data;                        // set EEPROM data
   ////EECR |= 0x04;                       // enable EEPROM for writing
   ////EECR |= 0x02;                       // write to EEPROM
   ////SREG = temp;                        // restore status register
////}
////
////char EEProm::ReadByteEE (int address)
   ////{
   ////unsigned char temp, data;           // local variables
   ////
   ////while (EECR & 0x02){}               // wait for EEPROM to be ready
   ////temp = SREG;                        // store status register
   ////cli ();									   // disable interrupts
   ////EEAR = address;                     // set EEPROM address
   ////EECR |= 0x01;                       // read EEPROM
   ////data = EEDR;                        // get data from EEPROM
   ////SREG = temp;                        // restore status register
   ////return (data);                       // return the EEPROM data
   ////}
////
////---------------------------------------------------------------------------------
 ////Basic run time routines
 ////Data Written Big endian here to conform to "tradition"
////
////void EEProm::WriteWordEE (int address, int data) 
   ////{  
   ////WriteByteEE (address,     data >> 8);
   ////WriteByteEE (address + 1, data & 0xff);    
   ////}   
    ////
////int EEProm::ReadWordEE (int address) 
   ////{
   ////return (ReadByteEE (address) << 8) + ReadByteEE (address + 1);
   ////}
   ////
////void EEProm::WriteDoubleEE (int address, double data)
  ////{
   ////tempf = data;
////
   ////unsigned char *ptr = (unsigned char*) &tempf;
   ////
   ////WriteByteEE (address,     ptr[0]);
   ////WriteByteEE (address + 1, ptr[1]);
   ////WriteByteEE (address + 2, ptr[2]);
   ////WriteByteEE (address + 3, ptr[3]);
   ////tempc [0] = ptr[0];      // Write bytes
   ////tempc [1] = ptr[1];
   ////tempc [2] = ptr[2];
   ////tempc [3] = ptr[3];
   ////
   ////}
    ////
////double EEProm::ReadDoubleEE  (int address)
  ////{
  ////tempf = data;
////
   ////char *ptr = (char*) &tempf; 
   ////
   ////ptr [0] = ReadByteEE (address);  
   ////ptr [1] = ReadByteEE (address + 1);   
   ////ptr [2] = ReadByteEE (address + 2);   
   ////ptr [3] = ReadByteEE (address + 3); 
     ////
   ////ptr[0] = tempc [0];    // Write bytes
   ////ptr[1] = tempc [1];
   ////ptr[2] = tempc [2];
   ////ptr[3] = tempc [3];
   ////
  ////return tempf;
  ////}
    ////