/*
 * Config.h
 *
 * Created: 5/25/2017 15:14:22
 *  Author: Fredc
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_


//#include <stdint-gcc.h>

#define  FIRMWARE_MAJOR_REV 5            // Firmware revision
#define  FIRMWARE_MINOR_REV 1            // Firmware revision




#define pH_DEFAULT_GAIN       -0.0128 // VI:123 
#define pH_DEFAULT_OFFSET     13.945
#define pH_DEFAULT_SETPOINT   7.5
#define pH_DEFAULT_HI_LIMIT   9.0
#define pH_DEFAULT_LO_LIMIT   7.0
//
//#define pH_DEFAULT_CAL_TARGET 7.4
//#define pH_DEFAULT_SAFETY_TIME  14
//#define pH_DEFAULT_PROP_LIMIT  35
//#define pH_DEFAULT_CAL_TARGET  7.4
//#define pH_MAX 14.0
//#define ph_MIN 0.0
//#define PH_DEFAULT_DEAD_ZONE  0.99

#define ORP_DEFAULT_GAIN       0.780
#define ORP_DEFAULT_OFFSET    199.4
#define ORP_DEFAULT_SETPOINT  700.0
#define ORP_DEFAULT_HI_LIMIT  800.0
#define ORP_DEFAULT_LO_LIMIT  500.0

//#define ORP_DEFAULT_SAFETY_TIME 25
//#define ORP_DEFAULT_PROP_LIMIT  35
//#define ORP_MAX 999.0
//#define ORP_MIN 0.0
//#define ORP_DEFAULT_DEAD_ZONE 0.99

#define PPM_DEFAULT_GAIN      1.0
#define PPM_DEFAULT_OFFSET    0
#define PPM_DEFAULT_SETPOINT  650
#define PPM_DEFAULT_HI_LIMIT  900
#define PPM_DEFAULT_LO_LIMIT  500

#define TDS_DEFAULT_GAIN       1.0
#define TDS_DEFAULT_OFFSET     0
#define TDS_DEFAULT_SETPOINT  50
#define TDS_DEFAULT_HI_LIMIT  100
#define TDS_DEFAULT_LO_LIMIT  0

#define TEMP_DEFAULT_GAIN      1.0
#define TEMP_DEFAULT_OFFSET    0
#define TEMP_DEFAULT_SETPOINT  72
#define TEMP_DEFAULT_HI_LIMIT  92
#define TEMP_DEFAULT_LO_LIMIT  52

#define Flow_DEFAULT_GAIN     1.0
#define Flow_DEFAULT_OFFSET   0.0
#define Flow_DEFAULT_SETPOINT   50.0 
#define Flow_DEFAULT_HI_LIMIT   1000.0
#define Flow_DEFAULT_LO_LIMIT   1000.0
//#define Flow_MINIMUM_REQUIRED  40.0

//#define DEFAULT_PROP_TIME  100
//#define DEFAULT_SETPOINT   50.0 
//#define DEFAULT_DEAD_ZONE  0.01

//#define DEFAULT_SAFETY_TIME     15
//#define DEFAULT_PROP_LIMIT 35




//===================================================
//enum Probe_Type {Probe_None, Probe_pH, Probe_ORP, Probe_Flow};
//enum Run_Mode   {Off_Mode, Man_Mode, Auto_Mode};  
      //
//#ifdef __AVR_ATmega88PA__
//#ifdef __AVR_ATtiny461__
//#ifdef __AVR_AT90CAN32__
//#ifdef __AVR_ATmega88__

//---------------------------------------------------------------

 ////Standard bit definitions
//
//#define BIT_0					0x01											
//#define BIT_1					0x02									
//#define BIT_2					0x04									
//#define BIT_3					0x08											
//#define BIT_4					0x10											
//#define BIT_5					0x20											
//#define BIT_6					0x40											
//#define BIT_7					0x80									
//
////#define byte uint8_t


// Status
//enum CStat {OFF, MAN, };


 
#endif /* CONFIG_H_ */