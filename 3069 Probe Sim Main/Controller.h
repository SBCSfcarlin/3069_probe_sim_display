/*
 * Controller.h
 *
 * Created: 7/28/2017 10:04:53
 *  Author: Fredc
 */ 


#include "Config.h"
#include "Sensor.h"
#include "Display.h"
#include "EEProm.h"
#include "PCBA_SIM18.h"
#include "Sensor.h"
#include "SPI.h"

//#include "Util.h"

#include "SCPI.h"

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#define BEEP_TIME 2
#define STATE_HOLD_TIME 1
#define BUTTON_HOLDDOWN_TRIP 25
#define BUTTON_TIMEOUT_TRIP  200

//#define SCPI_NUM_COMMANDS 21

class CController
   {
   public:
      CController ( );
      void Cont_Init ( );                 // Initialize Conductivity
      void Cont_Execute ( );              // Execute Conductivity Routine

      int pH;
      int ORP;
      int PPM;
      int TDS;
      int TEMP;
      int Aux;


      CSensor  pH_Sensor;
      CSensor  ORP_Sensor;
      CSensor  Flow_Sensor;  
      CSensor  PPM_Sensor;
      CSensor  TDS_Sensor;
      CSensor  Temp_Sensor; 

      CDisplay aDisplay;

      EEProm   aEEProm;
      Pulse_Wave_Modulator aPWM;
       
      //typedef void (CController::*pDebug_0) ( );
      //pDebug_0 Debug_0;
      // void Set_cb_ptr (pDebug_0);
      // void call_cb_func ( );

      
      //static void Debug_0 (char ch);          // pops out a debug message
      //static void Debug_0 (char *str);
      //static char * Debug_String;
      //static bool Debug_Flag;


    private:
      Serial_Port COM_0;                  // Com port 0
      Serial_Port COM_1;
      SPI_BUS SPI;
      
      int  oscnt;      // One second count
      int  ttcnt;      // timer tick count
      
      int spi_addr;
      int spi_offset;
      int spi_dac;

      int Read_Sensors (int scnt);
      //void Show_Button (Button btn);
      bool   Button_Action_Done;
      Button Last_Button_Pressed;
      Button Button_Pressed;
      bool   test_flag;
      int    PWM_Test;

   };




#endif /* CONTROLLER_H_ */

	  //CSCPI    aSCPI;
	  // aSCPI =  CSCPI ( );
	  
	  
	  
	  //int        Last_Func_Button_bit;       // Select a Function _BIT
	  //Probe_Type Last_Probe_Select;          // Select a Probe

	  // int Next_Display_State (int Disp_State, int buttons);
	  //int Next_Control_State (int Cont_State);
	  
	  //bool pH_Interlock_Armed;
	  //bool pH_Interlock_Tripped;
	  //bool Flow_Armed;
	  //bool Blink_Flag;
	  

	  //int  Beep_Time;
	  //int  Control_State;
	  //int  Display_State;
	  //int  Control_Hold;         // Short term time out
	  //bool Control_Run_Flag;     // Controller Run Flag
	  ////bool Mode_Update_Flag;     //


	  //void   SCPI_Init             (void);
	  //void   SCPI_Process          (char *str);
	  

	  
	  //   int    Last_Display_State;
	  // void   Show_Parameter (Probe_Type Probe, int Function);
	  //  void   Incr_Parameter (Probe_Type Probe, int Function);
	  // void   Decr_Parameter (Probe_Type Probe, int Function);
	  //  double Get_Parameter  (Probe_Type Probe, int Function);
	  //   void   Save_Parameter (Probe_Type Probe, int Function);              //Last_Func_Button_bit
	  //void   Re_Start  (void);
	  //
	  //int  buttons;                        // Button array pressed
	  //int  Last_Button_Array;              // Last button (s) pressed
	  //int  Button_Holddown_Count;          //
	  //int  Button_Timeout_Count;           // If no buttons pressed go back
	  //int  Button_Timeout_State;
	  //bool Button_Timeout_Flag;
	  //
	  //// bool If_Pressed (int Btn_Array, int Btn_Bit);   //
	  //bool Flow_OK (void);                // True if flow OK
	  //bool Flow_Alarm (void);             // True if Flow Alarm should be activated
	  //
	  //void Chirp (void);                  // Default 4
	  //void Beep (int B_Time);
	  //void Show_Dashes (void);
	  //
	  //void Show_Double (char *what, double val);
	  //void Show_Int (char *what, int val);
	  //void Show_Bool (char *what, bool val);
	  //
	  //char *Line1_buff; // [50];