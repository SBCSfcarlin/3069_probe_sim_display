/*
 * SPI.h
 *
 * Created: 11/6/2018 10:02:29
 *  Author: Fredc
 */ 


#ifndef SPI_H_
#define SPI_H_


#define SPI_BUFF_MAX 30

#define PH_ADDRESS      0        // Normal pH pwr board
#define ORP_ADDRESS     1        //
#define PPM_ADDRESS     1
#define ORP2_ADDRESS    2        // ORP Secondary not tested

#define PH2_ADDRESS     8        // Secondary PH on spi module

#define IN1_OUT6_ADDRESS 4       // SPI module

#define TDS_ADDRESS     12       // Secondary TDS
#define TEMP_ADDRESS    12       // Secondary Temp

#define PH_OFFSET   202
#define ORP_OFFSET  202

#define TDS_OFFSET  202
#define TEMP_OFFSET 204

#define PPM_OFFSET  204

#define IN1_OUT6_IN1   200

#define IN1_OUT6_OUT0    202
#define IN1_OUT6_OUT1    204
#define IN1_OUT6_OUT2    206
#define IN1_OUT6_OUT3    208
#define IN1_OUT6_OUT4    210
#define IN1_OUT6_OUT5    212

#define VERSION_OFFSET 199

//#if PC604 || (MODEL_PC == 2100)
//// For SpiState switch statement
//#define TASK_DELAY         0
//#define COND_ALL           1
//#define PPM                2
//#define SET_RELAYS_PWR_SUP 3
//#define SET_RELAYS_EXP_1   4
//#define SET_RELAYS_EXP_2   5
//#define SET_RELAYS_EXP_3   6
//#define GET_FLOW           7
//#define GET_420            8
//#define GET_420b           9
//#define SET_420            10
//#define SET_420b           11
//#define GET_604            12
//#define SET_RELAYS_EXP_4   13
//
//#endif
 //---------SPI BUS--------------------------------------------------------

 enum Spi_Command    {SPI_READ, SPI_WRITE};
 //#define SPI_BUFF_MAX 10

class SPI_BUS
   {
   public:
      void Init (void);
      int  Read (int addr, int offset);
      int  Read_Byte (int addr, int offset);
      // int  Read (int addr, int offset, int byte_count);

      int Write (int addr, int offset, int byte_count,  int value);

   protected:

   private:
      void sdly (void);
      //void Put (int chan1, int chan2, int chan3);
      bool Ready (void);
      int Send (int cData);                //Spi_Command    {SPI_READ, SPI_WRITE};
      int Transmit (Spi_Command SPI_C, int Spi_Address, int Spi_Offset, int byte_count, int value);

      int Spi_Buff [SPI_BUFF_MAX];
      int Spi_Buff_Temp;
      int *Spi_Buff_Pointer;
      int  Spi_Buff_Count;
      int  Get (int index);            // Get from buffer

      void Debug (char *str);
      char str [40];
   };



#endif /* SPI_H_ */