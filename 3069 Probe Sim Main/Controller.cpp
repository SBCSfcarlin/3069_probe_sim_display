/*
 * Controller.cpp
 *
 * Created: 7/28/2017 10:04:34
 *  Author: Fredc
 */ 

 #include <stdio.h>
  
 #include "Controller.h"
 #include "PCBA_SIM18.h"
 #include "Util.h"
 #include "EEProm.h"
 #include "Display.h"
 #include "SPI.h"

 //#include "Sensor.h"

 #include <stdfix.h>
  //#include "SCPI.h"

 #include "Test.h"

 #define F_CPU 8000000

 #include <util/delay.h>

  //bool CController::Debug_Flag = false;
 // char * CController::Debug_String = "DEBUG";

  CController::CController ( )
     {
     //pH_Sensor   = CSensor (Probe_pH);
     //ORP_Sensor  = CSensor (Probe_ORP);
     //Flow_Sensor = CSensor (Probe_Flow);
     //PPM_Sensor  = CSensor (Probe_PPM);
     //TDS_Sensor  = CSensor (Probe_TDS);
     //Temp_Sensor = CSensor (Probe_TEMP);

     //Last_Func_Button = Button ( );       // These may not be necessary. . .
     //Current_Probe = Probe_Type ( );
     aDisplay      = CDisplay ( );
     aEEProm       = EEProm   ( );
     aPWM          = Pulse_Wave_Modulator ( );
     SPI           = SPI_BUS  ( );
     
     }

 //--------------------------------------------------------------------
   //void CController::Debug_0 (char *str)
      //{
      ////COM_0.Send (str);
      //}
   //void CController::Debug_0 (char ch)
      //{
      ////COM_0.Send (ch);
      //}
//-------------------------------------------------------------------

   void CController::Cont_Init ( )        // Initialize Controller
      {
 
      PCBA2018::IO_Init ( );           // Initialize IO

      //pDebug_0 = &CController::Debug_0;
      test_flag     = false;
      SPI.Init ( );               // Initialize SPI Bus

      COM_0.Init (0, 9600, 100);        //void Init (int port, int baud_rate, int buffer_size);
      COM_1.Init (1, 9600, 100);        //void Init (int port, int baud_rate, int buffer_size);

      PCBA2018::Timer_Init ( );        // Initialize Timers
	   aPWM.Init ( );                   // Initialize Pulse_Wave_Modulator ( )
      PWM_Test = 0;
	   aEEProm.Init ( );
	   aDisplay.Init ( );             // Initialize Display

      pH_Sensor   = CSensor (Probe_pH,   pH_DEFAULT_GAIN,   pH_DEFAULT_OFFSET,   pH_DEFAULT_SETPOINT,   pH_DEFAULT_HI_LIMIT,   pH_DEFAULT_LO_LIMIT);
      ORP_Sensor  = CSensor (Probe_ORP,  ORP_DEFAULT_GAIN,  ORP_DEFAULT_OFFSET,  ORP_DEFAULT_SETPOINT,  ORP_DEFAULT_HI_LIMIT,  ORP_DEFAULT_LO_LIMIT);
      Flow_Sensor = CSensor (Probe_Flow, Flow_DEFAULT_GAIN, Flow_DEFAULT_OFFSET, Flow_DEFAULT_SETPOINT, Flow_DEFAULT_HI_LIMIT, Flow_DEFAULT_LO_LIMIT);
      PPM_Sensor  = CSensor (Probe_PPM,  PPM_DEFAULT_GAIN,  PPM_DEFAULT_OFFSET,  PPM_DEFAULT_SETPOINT,  PPM_DEFAULT_HI_LIMIT,  PPM_DEFAULT_LO_LIMIT);
      TDS_Sensor  = CSensor (Probe_TDS,  TDS_DEFAULT_GAIN,  TDS_DEFAULT_OFFSET,  TDS_DEFAULT_SETPOINT,  TDS_DEFAULT_HI_LIMIT,  TDS_DEFAULT_LO_LIMIT);
      Temp_Sensor = CSensor (Probe_TEMP, TEMP_DEFAULT_GAIN, TEMP_DEFAULT_OFFSET, TEMP_DEFAULT_SETPOINT, TEMP_DEFAULT_HI_LIMIT, TEMP_DEFAULT_LO_LIMIT);
      //
      COM_0.Send ("\r\nInit 9600 baud Com 0");
      COM_1.Send ("\r\nInit 9600 baud Com 1");

      spi_addr = 0; spi_offset = 200; 
      spi_dac = 0;
      //PCBA_CH250K::Spi_Init ( );          // Initialize Spi Bus
      ////PCBA_CH250K::WatchDog_Init ( );     // Initialize Watch dog
      
      Rho_Output::Init ( );
      oscnt = 0;                            // One second count
      ttcnt = 0;                            // Timer tick
      //// ADC_cnt = 0;                       // ADC counts  
      ////pH = 0.0;
      ////PCBA_CH250K::RS232_Send ("\r\n\r\n Power up Start");
      
     oscnt = 0;

     //SPI.Write (PH2_ADDRESS, 201, 1, 0);         // Write 0 to ranging bytes init PH
      }

 int CController::Read_Sensors (int scnt)  
    {                   // read sensors returns incremented sensor bookmark
    //int i;
    int raw;

    switch (scnt)
       {
       case 0:    // Different pH address
          // for (i = 0; i < 10; i++)
          //  {
           raw = SPI.Read (8, 202);   //8, 202
          //  if (raw != -1) break;
          //  }
       
         pH_Sensor.Raw (raw);
         break;
     case 1: 
        raw = SPI.Read (ORP_ADDRESS, ORP_OFFSET);
        ORP_Sensor.Raw (raw); 
        break;

     case 2: PPM_Sensor.Raw (SPI.Read (PPM_ADDRESS, PPM_OFFSET)); break;
     case 3: TDS_Sensor.Raw (SPI.Read (TDS_ADDRESS, TDS_OFFSET)); break;
     case 4: Temp_Sensor.Raw (SPI.Read (TEMP_ADDRESS, TEMP_OFFSET)); break;
     case 5: Aux = SPI.Read (IN1_OUT6_ADDRESS, IN1_OUT6_IN1);        break;
     case 6: break; // Must return on an even count
     default: return 0; break;
     }
   return (scnt + 1);
   }

  //----------------------------------------------------------------
   //void CController::Show_Button (Button btn)
      //{
      //char *str8;
//
      //switch (btn)
           //{
              //case NONE:    str8 = "NONE   "; break;
              //case UP:      str8 = "UP     "; break;
              //case DOWN:    str8 = "DOWN   "; break;
              //case LEFT:    str8 = "LEFT   "; break;
              //case RIGHT:   str8 = "RIGHT  "; break;
              //case OK:      str8 = "OK     "; break;
              //case MYSTERY: str8 = "MYSTERY"; break;
           //}
        //COM_0.Send (str8);
        //COM_1.Send (str8);
        //}
  //-------------------------------------------------------------- 
      
  void CController::Cont_Execute ( )              // Execute Controller Routine
     {
     bool osf;             // one second flag
     bool ttf;             // timer tickflag;

     char str [40];
     
     int ch;
     int j;
     int raw;              // Raw counts
     int hi;
     int lo;
     double pHf;
     int i;
    
    COM_0.Tx_Check ( );
    COM_0.Rx_Check ( );
    COM_1.Tx_Check ( );
    COM_1.Rx_Check ( );

    osf = PCBA2018::Get_One_Second_Flag ( );              // One second Timer 
    ttf = PCBA2018::Get_Timer_Tick ( ); 

    if (ttf)
       {
       Button_Pressed = Buttons::Read_Buttons ( );
           // Last_Button_Pressed = NONE;
          //  Button_Action_Done = true;

       ttcnt += 1;
       test_flag = !test_flag;

       PCBA2018::Test (test_flag);     // Test the timer tick
       Rho_Output::Test (test_flag );           // Strobe Rho
      // ttcnt = Read_Sensors (ttcnt);   // Read sensors , update  count
      
     
      
      // if (ttcnt > 100) ttcnt = 0;

       //if (Util::Debug_Flag == true)
          //{
          //COM_0.Send (Util::Debug_String);
          //Util::Debug_Flag = false;
          //}
       }

    if (osf)
       {
       oscnt += 1;   
       PWM_Test += 10;
       if (PWM_Test >= 1023) PWM_Test = 0;
       aPWM.Set (PWM_Test) ;
       sprintf (str, "\r\n                 PWM Set %d", PWM_Test);
       COM_0.Send (str); 
       //}
      // Show_Button (Button_Pressed);                         

       //if (oscnt > 22) oscnt = 11;
       switch (Button_Pressed)
          {
          case MYSTERY: 
             sprintf (str,"~[2J");            // Erase screen
              COM_1.Send (str);
              break;
          case OK:
             sprintf (str, "123456789 ");       // Blank line at bottom
             COM_1.Send (str);
             break;
          }

                   
      
       switch (oscnt)
          {
          case 5: 
             COM_0.Send ("\r\nThis is Com 0");

             sprintf (str,"~[2J");            // ERase screen
             COM_1.Send (str);
             COM_1.Send ("\r\nThis is Com 1--");
             break;

          case 10:
             //PCBA2018::Test (oscnt & 1);      // Stub for test code
             
             break;
             
         case 11:
            sprintf (str,"~[2J");            // ERase screen
            COM_1.Send (str);

            sprintf (str, "_   _   _   _ ");       // Blank line at bottom
            COM_1.Send (str);

            //spi_offset = 202;               // Update board
            //spi_addr = 4;
            //spi_dac += 10; if (spi_dac > 1024) spi_dac = 0; 
            //SPI.Write(spi_addr, spi_offset, 2, spi_dac);
            //sprintf (str, "[%d %d] = %d ", spi_addr, spi_offset, spi_dac);
            COM_0.Send (str);              // 
            break;
      
         case 12:
             sprintf (str, "~[1;1");   COM_1.Send (str);
             sprintf (str, "\r\nChemtrol Simulator");
             COM_1.Send (str);
             COM_0.Send (str);
             break;

         case 13:
             //SPI.Write(PH2_ADDRESS, 201, 1, 0);         // Write 0 to ranging bytes
             //SPI.Init ( );                              // TRy re-init
             //raw = SPI.Read (PH2_ADDRESS, PH_OFFSET);   //8, 204
             //for (i = 0; i < 10; i++)
                //{
                //raw = SPI.Read (8, 202);   //8, 202
                //if (raw != -1) break;
                //}
             //
             //pH_Sensor.Raw (raw);  
             pHf = pH_Sensor.Current ( );
             hi = (int) pHf;
             lo =  (pHf * 10) - (10 * hi); 
                
             sprintf (str, "\r\npH %d.%d ", hi, lo);
             //COM_0.Send (str);
             sprintf (str, "~[1;1");   COM_1.Send (str); 
             sprintf (str, "\r\npH %d.%d", hi, lo);
             COM_1.Send (str); 
             COM_0.Send (str);   
             break;


          case 14:
             //raw = SPI.Read (ORP_ADDRESS, ORP_OFFSET);
             //ORP_Sensor.Raw (raw);
             ORP = ORP_Sensor.Current ( );
             sprintf (str, "\r\nORP %d ", ORP);
             COM_0.Send (str); 
             sprintf (str, "~[2;2");   COM_1.Send (str);
             sprintf (str, "\r\nORP %d", ORP);
             COM_1.Send (str);   
             break;

          case 15:
             //PPM_Sensor.Raw (SPI.Read (PPM_ADDRESS, PPM_OFFSET));
             PPM = PPM_Sensor.Current ( );
             sprintf (str, "\r\nPPM %d ", PPM);
             COM_0.Send (str); 
             sprintf (str, "~[3;3");   COM_1.Send (str);
             sprintf (str, "\r\nPPM %d", PPM);
             COM_1.Send (str);
             break;

          case 16:
             //TDS_Sensor.Raw (SPI.Read (TDS_ADDRESS, TDS_OFFSET));
             TDS = TDS_Sensor.Current ( );
             sprintf (str, "\r\nTDS %d ", TDS);
             COM_0.Send (str);
             sprintf (str, "~[4;4");   COM_1.Send (str);
             sprintf (str, "\r\nTDS %d", TDS);
             COM_1.Send (str);   
             break;
             
 

          case 17:            
             //Temp_Sensor.Raw (SPI.Read (TEMP_ADDRESS, TEMP_OFFSET));
             TEMP = Temp_Sensor.Current ( );
             sprintf (str, "\r\nTEMP %d ", TEMP);
             COM_0.Send (str);
             sprintf (str, "~[5;5");   COM_1.Send (str);
             sprintf (str, "\r\nTEMP %d", TEMP);
             COM_1.Send (str);   
             break;

         case 18:
             //Aux = SPI.Read (IN1_OUT6_ADDRESS, IN1_OUT6_IN1);
             //sprintf (str, "\r\nAUX_IN   %d ", Aux);
             //COM_0.Send (str);
             //sprintf (str, "\r\nAUX in %d", Aux);
             //COM_1.Send (str);
            break;
       case 19:
          sprintf (str, "\r\n\r\n");
          COM_0.Send (str);
          break;

      default:
        oscnt = 10;
        break;       
        }

      //oscnt += 1;       // 10 gets bumped to 11
      } // if (osf)

     
     ch = COM_0.Get_Char ( );
     if (ch != '\0') 
         {
         COM_0.Send ("\r\n");      // Local echo
         COM_0.Send (ch);      // Local echo
         COM_1.Send (ch);      // Send to Display
         }
    
     } // End Cont Execute
    
    //=================================================================

     //ORP_Sensor.Raw (PCBA_CH250K::Spi_Read_ORP ( ));
     //ORP_Sensor.Check_Safety_Timer (3);
     //ORP_Sensor.Check_Prop_Mode (3);
     //if (ORP_Sensor.Get_Mode ( ) == Off_Mode) {ORP_Sensor.Reset_Safety_Timer ( );}
     ////sprintf (str, "\r\n ORP: %d", (int) ORP_Sensor.Current()); PCBA_CH250K::RS232_Send (str);
     //aDisplay.Disp_ORP (Util::Bracket (ORP_Sensor.Current ( ), 999.0, 0.0));
     ////aDisplay.Disp_ORP (ORP_Sensor.Current ( ));
     //if (pH_Sensor.Safety_Timer_Tripped)   aDisplay.Disp_pH ("St");


     //==========================================================

     //bool osf;             // One second flag
     //bool hsf;             // Half second flag
     //bool ttf;             // Timer tick flag   
     //bool btf;             // Button tick flag
     //bool dtf;             // Display tick flag
     //char ch;
     //char *str;
     //char str [30];
     //int i;

     
     
     //osf = PCBA_CH250K::Get_Timer_Flag ( );         // One second flag
    //// hsf = PCBA_CH250K::Get_Half_Second_Flag ( );   // One second flag
     //dtf = PCBA_CH250K::Get_Display_Flag ( );       // Display flag
     //ttf = PCBA_CH250K::Get_Timer_Tick ( );         // Timer tick   
     //btf = PCBA_CH250K::Get_Button_Flag ( );        // Button flag

     //PCBA_CH250K::RS232_Check_Rx ( );          // Check input echo
     
     //if (ttf)                   // Timer Tick
        //{
        //ttcnt += 1;
    //    PCBA_CH250K::RS232_Check_Tx ( );          // Check RS232 Transmit
        
        
        //if (PCBA_CH250K::Line1_Ready ( ))
           //{
           //for (i = 0; i < 30; i++)
              //{
              //str [i] = PCBA_CH250K::Get_Line1  ( );
              //}           
           //
           ////PCBA_CH250K::RS232_Send (str);
           //SCPI::SCPI_Process (str);
           //}
                      
  //     if (PCBA_CH250K::Line1_Ready ( ))
     //     {
         // Line1_buff = PCBA_CH250K::Get_Line1 ( );
         // PCBA_CH250K::RS232_Send (Line1_buff); ;// Local echo
     //     aSCPI.SCPI_Process (Line1_buff); zzz
       //   }          
        //}  // End ttf   
      
         
  //    buttons = PCBA_CH250K::Button_Array ( );     // Get the buttons
      //if (btf)                                     // Check button timer flag
         //{
      ////   PCBA_CH250K::Button_Check ( );
         //
         //switch (Button_Timeout_State)
            //{
            //case 0: 
               //if (Button_Timeout_Count > 0)       // The Counter has been started
                  //Button_Timeout_State = 1;
               //break;
            //case 1: 
               //Button_Timeout_Count -= 1;          // Drop count
               //if (Button_Timeout_Count == 0)      // If we are timed out
                  //{
                  //Button_Timeout_Flag = true;      // Raise the flag
                  //Button_Timeout_State = 0;        // Reset the counter
                  ////PCBA_CH250K::RS232_Send ("\r\nButton Time Out!");
                  //}
               //break;  
            //default: 
               //Button_Timeout_State = 0; 
               //Button_Timeout_Count = 0; 
               //break;
            //}
            
         //   buttons =  PCBA_CH250K::Button_Array ( );
          //Show_Int ("\r\nButtons Array ", buttons);
         //if ((Last_Button_Array != buttons) || Button_Timeout_Flag)
            //{
             
           // Display_State = Next_Display_State (Display_State, buttons);      // Display always runs
            //Last_Button_Array = buttons; 
            //Button_Timeout_Flag = false;                             // If the timeout flag had been set, clear it
            //
            ////sprintf (str, "\r\n Buttons = %x", buttons);
            //PCBA_CH250K::RS232_Send (str);
            //Last_Button_Array = buttons;
            //Button_Holddown_Count = 0;             // Clear the hold down count
            //if (buttons != 0) 
               //{
            ////   PCBA_CH250K::RS232_Send ("Button Chirp"); 
               ////Chirp ( ); 
               //}              
            //}
            
         //if (Last_Display_State != Display_State)
            //{
            ////sprintf (str, "\r\nDisplay State = %d", Display_State);
            ////PCBA_CH250K::RS232_Send (str);
            //Last_Display_State = Display_State;
            //}    
            //
         //if (Last_Button_Array == buttons)
            //{
            //Button_Holddown_Count += 1;         // Bump up count  
            //if (Button_Holddown_Count > BUTTON_HOLDDOWN_TRIP)
               //{
               //if ((buttons & BTN_ORP_BIT) !=  0) 
                  //{ORP_Sensor.Set_Default_Parameters ( ); Chirp ( );}  // PCBA_CH250K::RS232_Send ("\r\nReset ORP"); 
               //if ((buttons & BTN_PH_BIT)  !=  0) 
                  //{pH_Sensor.Set_Default_Parameters ( );  Chirp ( );}  // PCBA_CH250K::RS232_Send ("\r\nReset pH");
               //}
            //}
         
        //if (Beep_Time > 0) Beep_Time -= 1;
     //   if (Beep_Time <= 0) PCBA_CH250K::Digital_Out (DOUT_BUZZER, false);
     //  }  // end in (btf)          
           
       
     //if (dtf)                         // Check display timer flag
        //{
        //aDisplay.Disp_UPD ( );        // Update display               
        //}  // end dtf
        //
             //
      //if (osf)                      // pick osf or hsf
         //{        
         ////PCBA_CH250K::WatchDog_Touch ( );
         ////=======================================Who let the dogs out?=============================================   
         //oscnt += 1;
         //oscnt &= 0x3fff;
         ////Blink_Flag = !Blink_Flag;                 // Flip the flag
         //Control_State = Next_Control_State (Control_State);
        //} 
        
     //} // End Cont Execute
     
  //========================================
   //---------------------------Helper Methods-------------------------------------------------------------
   
   //void CController::Chirp (void)
   //{
	 ////  PCBA_CH250K::Digital_Out (DOUT_BUZZER, true);
	   //if (Beep_Time ==  0)              // If Beep in progress, let it go;
	   //Beep_Time = BEEP_TIME;         // Otherwise, hit it!
   //}
   //
   //void CController::Beep (int B_Time)     //
   //{
	  //// PCBA_CH250K::Digital_Out (DOUT_BUZZER, true);
	   //Beep_Time = B_Time;
   //}
   //
   //bool CController::Flow_OK ( )
   //{
	 ////  if (PCBA_CH250K::Read_Flow ( ) >= Flow_MINIMUM_REQUIRED)  return true;
	   //return false;
   //}
   //
   //bool CController::Flow_Alarm (void)            // No flow alarm if probes off
   //{                                           // No alarms in manual or off
	   ////if (!Flow_OK ( ) &&                        // IV:5
	   ////(
	   //////(ORP_Sensor.Get_Mode ( ) != Off_Mode) |
	   //////(pH_Sensor.Get_Mode ( )  != Off_Mode)
	   ////)
	   ////)
	   ////return true;
	   //return false;
   //}
   //

   
   //bool CController::If_Pressed (int Btn_Array, int Btn_Bit)
   //{
   //if ((Btn_Array & Btn_Bit) != 0) return true;
   //return false;
   //}
   
   //void CController::Show_Dashes (void)
   //{
	   ////aDisplay.Disp_ORP ("---");
	   ////aDisplay.Disp_pH ("--");
   //}
   //
   //void CController::Re_Start  (void)
   //{
	   //
	   ////pH_Sensor.Set_Set_Point   ( aEEProm.Get_pH_SetPoint ( ));
	   ////pH_Sensor.Set_Hi_Limit    (aEEProm.Get_pH_Hi_Limit ( ));
	   ////pH_Sensor.Set_Lo_Limit    (aEEProm.Get_pH_Lo_Limit ( ));
	   ////pH_Sensor.Set_Safety_Time_Limit (aEEProm.Get_pH_Safety_Limit ( ));
	   ////pH_Sensor.Set_Prop_Limit  (aEEProm.Get_pH_Prop_Limit ( ));
	   ////pH_Sensor.Set_Cal_Target  ( aEEProm.Get_pH_Cal_Target ( ));
	   ////
	   ////ORP_Sensor.Set_Set_Point   (aEEProm.Get_ORP_SetPoint ( ));
	   ////ORP_Sensor.Set_Hi_Limit    (aEEProm.Get_ORP_Hi_Limit ( ));
	   ////ORP_Sensor.Set_Lo_Limit    (aEEProm.Get_ORP_Lo_Limit ( ));
	   ////ORP_Sensor.Set_Safety_Time_Limit (aEEProm.Get_ORP_Safety_Limit ( ));
	   ////ORP_Sensor.Set_Prop_Limit  (aEEProm.Get_ORP_Prop_Limit ( ));
	   ////
	   //
//
	   //
	   ////EEProm::Get_Flow_SetPoint ( );
	   ////EEProm::Get_Flow_Hi_Limit ( );
	   ////EEProm::Get_Flow_Lo_Limit ( );
   //}
   //
   //void CController::Show_Parameter (Probe_Type Probe, int Function) //Last_Func_Button_bit
   //{
	   //switch (Probe)
	   //{
		   //case Probe_pH:
		   //switch (Function)
		   //{
			   ////case BTN_LOLIM_BIT:  aDisplay.Disp_pH (pH_Sensor.Get_Lo_Limit ( ));          break;
			   ////case BTN_HILIM_BIT:  aDisplay.Disp_pH (pH_Sensor.Get_Hi_Limit ( ));          break;
			   ////case BTN_PROP_BIT:   aDisplay.Disp_HH (pH_Sensor.Get_Prop_Limit ( ));        break;
			   ////case BTN_SAFE_BIT:   aDisplay.Disp_HH (pH_Sensor.Get_Safety_Time_Limit ( )); break;
			   ////case BTN_SET_BIT:    aDisplay.Disp_pH (pH_Sensor.Get_Set_Point ( ));         break;
			   ////case BTN_PHCAL_BIT:  aDisplay.Disp_pH (pH_Sensor.Get_Cal_Target ( ));        break;
		   //}
		   //break;
		   //
		   //case Probe_ORP:
		   //switch (Function)
		   //{
			   ////case BTN_LOLIM_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Get_Lo_Limit ( ));          break;
			   ////case BTN_HILIM_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Get_Hi_Limit ( ));          break;
			   ////case BTN_PROP_BIT:   aDisplay.Disp_ORP (ORP_Sensor.Get_Prop_Limit ( ));        break;
			   ////case BTN_SAFE_BIT:   aDisplay.Disp_ORP (ORP_Sensor.Get_Safety_Time_Limit ( )); break;
			   ////case BTN_SET_BIT:    aDisplay.Disp_ORP (ORP_Sensor.Get_Set_Point ( ));         break;
			   //// case BTN_PHCAL_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Get_Cal_Target ( ));       break;
		   //}
		   //break;
	   //}
   //}
   //
   //void CController::Incr_Parameter (Probe_Type Probe, int Function)             //Last_Func_Button_bit
   //{
	   //double incr;
	   //
	   //switch (Probe)
	   //{
		   //case Probe_pH:
		   ////switch (Function)
		   ////{
		   ////case BTN_LOLIM_BIT: if (pH_Sensor.Get_Lo_Limit ( ) >= 10.0) incr = 1.0; else incr = 0.1;
		   ////aDisplay.Disp_pH (pH_Sensor.Inc_Lo_Limit (incr, pH_MAX, ph_MIN));         break;
		   ////case BTN_HILIM_BIT:  if (pH_Sensor.Get_Hi_Limit ( ) >= 10.0) incr = 1.0; else incr = 0.1;
		   ////aDisplay.Disp_pH (pH_Sensor.Inc_Hi_Limit (incr, pH_MAX, ph_MIN));         break;
		   ////case BTN_PROP_BIT:   aDisplay.Disp_HH (pH_Sensor.Inc_Prop_Limit (1.0));        break;
		   ////case BTN_SAFE_BIT:   aDisplay.Disp_HH (pH_Sensor.Inc_Safety_Time_Limit (1.0)); break;
		   ////case BTN_SET_BIT:    if (pH_Sensor.Get_Set_Point ( ) >= 10.0) incr = 1.0; else incr = 0.1;
		   ////aDisplay.Disp_pH (pH_Sensor.Inc_Set_Point (incr, pH_MAX, ph_MIN));         break;
		   ////case BTN_PHCAL_BIT:  if (pH_Sensor.Get_Cal_Target ( ) >= 10.0) incr = 1.0; else incr = 0.1;
		   ////aDisplay.Disp_pH (pH_Sensor.Inc_Cal_Target (incr, pH_MAX, ph_MIN));        break;
		   ////}
		   //break;
		   //
		   //case Probe_ORP:
		   //switch (Function)
		   //{
			   ////case BTN_LOLIM_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Inc_Lo_Limit (1.0, ORP_MAX, ORP_MIN));           break;
			   ////case BTN_HILIM_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Inc_Hi_Limit (1.0, ORP_MAX, ORP_MIN));           break;
			   ////case BTN_PROP_BIT:   aDisplay.Disp_ORP (ORP_Sensor.Inc_Prop_Limit (1.0));         break;
			   ////case BTN_SAFE_BIT:   aDisplay.Disp_ORP (ORP_Sensor.Inc_Safety_Time_Limit (1.0));  break;
			   ////case BTN_SET_BIT:    aDisplay.Disp_ORP (ORP_Sensor.Inc_Set_Point (1.0, ORP_MAX, ORP_MIN));          break;
			   ////case BTN_PHCAL_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Inc_Calibration (1.0);        break;
		   //}
		   //break;
	   //}
   //}
   //
   //void CController::Decr_Parameter (Probe_Type Probe, int Function) //Last_Func_Button_bit
   //{
	   //double incr;
	   //
	   //switch (Probe)
	   //{
		   //case Probe_pH:
		   //switch (Function)
		   //{
			   ////case BTN_LOLIM_BIT:  if (pH_Sensor.Get_Lo_Limit ( ) >= 10.0) incr = -1.0; else incr = -0.1;
			   ////aDisplay.Disp_pH (pH_Sensor.Inc_Lo_Limit (incr, pH_MAX, ph_MIN));          break;
			   ////case BTN_HILIM_BIT:  if (pH_Sensor.Get_Hi_Limit ( ) >= 10.0) incr = -1.0; else incr = -0.1;
			   ////aDisplay.Disp_pH (pH_Sensor.Inc_Hi_Limit (incr, pH_MAX, ph_MIN));          break;
			   ////case BTN_PROP_BIT:   aDisplay.Disp_HH (pH_Sensor.Inc_Prop_Limit (-1.0));        break;
			   ////case BTN_SAFE_BIT:   aDisplay.Disp_HH (pH_Sensor.Inc_Safety_Time_Limit (-1.0)); break;
			   ////case BTN_SET_BIT:    if (pH_Sensor.Get_Set_Point ( ) >= 10.0) incr = -1.0; else incr = -0.1;
			   ////aDisplay.Disp_pH (pH_Sensor.Inc_Set_Point (incr, pH_MAX, ph_MIN));         break;
			   ////case BTN_PHCAL_BIT:  if (pH_Sensor.Get_Cal_Target ( ) >= 10.0) incr = -1.0; else incr = -0.1;
			   ////aDisplay.Disp_pH (pH_Sensor.Inc_Cal_Target (incr, pH_MAX, ph_MIN));        break;
		   //}
		   //break;
		   //
		   //case Probe_ORP:
		   //switch (Function)
		   //{
			   ////case BTN_LOLIM_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Inc_Lo_Limit (-1.0, ORP_MAX, ORP_MIN));           break;
			   ////case BTN_HILIM_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Inc_Hi_Limit (-1.0, ORP_MAX, ORP_MIN));           break;
			   ////case BTN_PROP_BIT:   aDisplay.Disp_ORP (ORP_Sensor.Inc_Prop_Limit (-1.0));         break;
			   ////case BTN_SAFE_BIT:   aDisplay.Disp_ORP (ORP_Sensor.Inc_Safety_Time_Limit  (-1.0)); break;
			   ////case BTN_SET_BIT:    aDisplay.Disp_ORP (ORP_Sensor.Inc_Set_Point (-1.0, ORP_MAX, ORP_MIN));          break;
			   //////case BTN_PHCAL_BIT:  aDisplay.Disp_ORP (ORP_Sensor.Inc_Calibration (-1.0));        break;
		   //}
		   //break;
	   //}
   //}
   //----------------------
   //double CController::Get_Parameter (Probe_Type Probe, int Function)             //Last_Func_Button_bit
   //{
	   //switch (Probe)
	   //{
		   //case Probe_pH:
		   //switch (Function)
		   //{
			   //case BTN_LOLIM_BIT:  return pH_Sensor.Get_Lo_Limit ( );          break;
			   //case BTN_HILIM_BIT:  return pH_Sensor.Get_Hi_Limit ( );          break;
			   //case BTN_PROP_BIT:   return pH_Sensor.Get_Prop_Limit ( );        break;
			   //case BTN_SAFE_BIT:   return pH_Sensor.Get_Safety_Time_Limit ( ); break;
			   //case BTN_SET_BIT:    return pH_Sensor.Get_Set_Point ( );         break;
			   //case BTN_PHCAL_BIT:  return pH_Sensor.Get_Cal_Target  ( );       break;
		   //}
		   //break;
		   //
		   //case Probe_ORP:
		   //switch (Function)
		   //{
			   //case BTN_LOLIM_BIT: return ORP_Sensor.Get_Lo_Limit ( );           break;
			   //case BTN_HILIM_BIT: return ORP_Sensor.Get_Hi_Limit ( );           break;
			   //case BTN_PROP_BIT:  return ORP_Sensor.Get_Prop_Limit ( );         break;
			   //case BTN_SAFE_BIT:  return ORP_Sensor.Get_Safety_Time_Limit ( );  break;
			   //case BTN_SET_BIT:   return ORP_Sensor.Get_Set_Point ( );          break;
			   //// case BTN_PHCAL_BIT: return ORP_Sensor.  break;
		   //}
		   //break;
	   //}
	   //return 0.0;
   //}
   
   //void CController::Save_Parameter (Probe_Type Probe, int Function)         //Last_Func_Button_bit
   //{
	   //switch (Probe)
	   //{
		   //case Probe_pH:
		   //switch (Function)
		   //{
			   //case BTN_LOLIM_BIT: aEEProm.Save_pH_Lo_Limit (pH_Sensor.Get_Lo_Limit ( ));                    break;
			   //case BTN_HILIM_BIT: aEEProm.Save_pH_Hi_Limit (pH_Sensor.Get_Hi_Limit ( ));                    break;
			   //case BTN_PROP_BIT:  aEEProm.Save_pH_Prop_Limit (pH_Sensor.Get_Prop_Limit ( ));                break;
			   //case BTN_SAFE_BIT:  aEEProm.Save_pH_Safety_Limit (pH_Sensor.Get_Safety_Time_Limit ( ));       break;
			   //case BTN_SET_BIT:   aEEProm.Save_pH_SetPoint (pH_Sensor.Get_Set_Point ( ));                   break;
			   //case BTN_PHCAL_BIT: aEEProm.Save_pH_Cal_Target (pH_Sensor.Get_Cal_Target ( ));
			   //pH_Sensor.Do_Calibration ( );                                             break;
		   //}
		   //break;
		   //
		   //switch (Function)
		   //{
			   //case BTN_LOLIM_BIT:  aEEProm.Save_ORP_Lo_Limit (ORP_Sensor.Get_Lo_Limit ( ));               break;
			   //case BTN_HILIM_BIT:  aEEProm.Save_ORP_Hi_Limit (ORP_Sensor.Get_Hi_Limit ( ));               break;
			   //case BTN_PROP_BIT:   aEEProm.Save_ORP_Prop_Limit (ORP_Sensor.Get_Prop_Limit ( ));           break;
			   //case BTN_SAFE_BIT:   aEEProm.Save_ORP_Safety_Limit (ORP_Sensor.Get_Safety_Time_Limit ( ));  break;
			   //case BTN_SET_BIT:    aEEProm.Save_ORP_SetPoint (ORP_Sensor.Get_Set_Point ( ));              break;
			   ////case BTN_PHCAL_BIT:  aEEProm.Save_pH_Lo_Limit (ORP_Sensor.Get_Lo_Limit ( ));               break;
		   //}
		   //break;
	   //}
   //}
   
   // ---------------------------------Diagnostic stuff-----------------------------------
   //void CController::Show_Double (char *what, double val)
   //{
	   //// sprintf (str, "\r\n %s, %f", what, val);
	   //// PCBA_CH250K::RS232_Send (str);
   //}
   //
   //void CController::Show_Int (char *what, int val)
   //{
	   ////sprintf (str, "\r\n %s, %d", what, val);
	   //// PCBA_CH250K::RS232_Send (str);
   //}
   //
   //void CController::Show_Bool (char *what, bool val)
   //{
	   //// if (val) sprintf (str, "\r\n %s, true", what);
	   ////  else  sprintf (str, "\r\n %s, false", what);
	   //// PCBA_CH250K::RS232_Send (str);
   //}
   
  //==================================================================================
  //int CController::Next_Control_State (int Cont_State)
  //{
	  //int NS;
	  //bool alarm;
	  //
	  //NS = 0;
	  ////PCBA_CH250K::RS232_Check_Tx ( );          // Check RS232 Transmit
	  ////if (Mode_Update_Flag)
	  ////{
		  ////aDisplay.Disp_Mode (Probe_ORP, ORP_Sensor.Get_Mode ( ));
		  ////aEEProm.Save_ORP_Mode (ORP_Sensor.Get_Mode ( ));
////
		  ////aDisplay.Disp_Mode (Probe_pH, pH_Sensor.Get_Mode ( ));
		  ////aEEProm.Save_pH_Mode (pH_Sensor.Get_Mode ( ));
		  ////Mode_Update_Flag = false;
	  ////}
	  //
	  //switch (Cont_State)
	  //{
		  //case 0:
		  //NS = 10;
		  //Control_Hold = STATE_HOLD_TIME;
		  //aDisplay.LED_Auto   (Probe_ORP, true, false);
		  //aDisplay.LED_Off    (Probe_ORP, true, false);
		  //aDisplay.LED_Man    (Probe_ORP, true, false);
		  //aDisplay.LED_Feed   (Probe_ORP, true, false);
		  //aDisplay.LED_Alarm  (Probe_ORP, true, false);
		  //aDisplay.LED_Auto   (Probe_pH, true,  false);
		  //aDisplay.LED_Off    (Probe_pH, true,  false);
		  //aDisplay.LED_Man    (Probe_pH, true,  false);
		  //aDisplay.LED_Feed   (Probe_pH, true,  false);
		  //aDisplay.LED_Alarm  (Probe_pH, true,  false);
//
		  //aDisplay.Disp_Put (0, '8', true);
		  //aDisplay.Disp_Put (1, '8', true);
		  //aDisplay.Disp_Put (2, '8', true);
		  //aDisplay.Disp_Put (3, '8', true);
		  //aDisplay.Disp_Put (4, '8', true);
		  //aDisplay.Disp_Put (5, '8', true);
		  ////PCBA_CH250K::RS232_Send ("\r\n Power up Start");
		  //
		  ////
		  ////pH_Sensor.Set_Set_Point (aEEProm.Get_pH_SetPoint ( ));
		  ////pH_Sensor.Set_Hi_Limit  (aEEProm.Get_pH_Hi_Limit ( ));
		  ////pH_Sensor.Set_Lo_Limit  (aEEProm.Get_pH_Lo_Limit ( ));
		  ////pH_Sensor.Set_Safety_Time_Limit  (aEEProm.Get_pH_Safety_Limit ( ));
		  ////pH_Sensor.Set_Prop_Limit  (aEEProm.Get_pH_Prop_Limit ( ));
		  //////
		  ////ORP_Sensor.Set_Set_Point (aEEProm.Get_ORP_SetPoint ( ));
		  ////ORP_Sensor.Set_Hi_Limit  (aEEProm.Get_ORP_Hi_Limit ( ));
		  ////ORP_Sensor.Set_Lo_Limit  (aEEProm.Get_ORP_Lo_Limit ( ));
		  ////ORP_Sensor.Set_Safety_Time_Limit  (aEEProm.Get_ORP_Safety_Limit ( ));
		  ////ORP_Sensor.Set_Prop_Limit  (aEEProm.Get_ORP_Prop_Limit ( ));
		  //////
		  ////Flow_Sensor.Set_Set_Point (aEEProm.Get_Flow_SetPoint ( ));
		  ////Flow_Sensor.Set_Hi_Limit  (aEEProm.Get_Flow_Hi_Limit ( ));
		  ////Flow_Sensor.Set_Lo_Limit  (aEEProm.Get_Flow_Lo_Limit ( ));
		  ////
		  //break;
		  //
		  //case 10:
		  //NS = 10;
		  //aDisplay.LED_Auto   (Probe_ORP, false, false);
		  //aDisplay.LED_Off    (Probe_ORP, false, false);
		  //aDisplay.LED_Man    (Probe_ORP, false, false);
		  //aDisplay.LED_Feed   (Probe_ORP, false, false);
		  //aDisplay.LED_Alarm  (Probe_ORP, false, false);
		  //aDisplay.LED_Auto   (Probe_pH,  false, false);
		  //aDisplay.LED_Off    (Probe_pH,  false, false);
		  //aDisplay.LED_Man    (Probe_pH,  false, false);
		  //aDisplay.LED_Feed   (Probe_pH,  false, false);
		  //aDisplay.LED_Alarm  (Probe_pH,  false, false);
		  //
		  //
		  ////ORP_Sensor.Set_Mode (aEEProm.Get_ORP_Mode  ( ) );          // Restore Mode
		  ////pH_Sensor.Set_Mode  (aEEProm.Get_pH_Mode  ( ) );
		  ////
		  ////aDisplay.Disp_Mode (Probe_ORP, ORP_Sensor.Get_Mode ( ));
		  ////aDisplay.Disp_Mode (Probe_pH, pH_Sensor.Get_Mode ( ));
//
		  ////aDisplay.Disp_Mode (Probe_pH, pH_Sensor.Set_Mode (Off_Mode));
		  ////aDisplay.Disp_Mode (Probe_ORP, ORP_Sensor.Set_Mode (Off_Mode));
		  //
		  //Control_Hold = STATE_HOLD_TIME;
		  //
		  //if (Control_Run_Flag) NS = 20;
		  //break;
		  //
		  //case 20:
		  //aDisplay.Disp_ORP (100 * FIRMWARE_MAJOR_REV + FIRMWARE_MINOR_REV);
		  //aDisplay.Disp_pH ("00");
		  //Control_Hold -= 1;
		  //if (Control_Hold <= 0)
		  //{
			  //Control_Hold = STATE_HOLD_TIME;
			  //NS = 21;
		  //}
		  //else NS = 20;
		  //break;
		  //
		  //case 21:
		  //aDisplay.Disp_ORP ("PHf");
		 //// if (pH_Sensor.Get_Reverse_Feed ( )) aDisplay.Disp_pH ("ac");
		 //// else aDisplay.Disp_pH ("ba");
		  //Control_Hold -= 1;
		  //if (Control_Hold <= 0)
		  //{
			  //Control_Hold = STATE_HOLD_TIME;
			  //NS = 22;
		  //}
		  //else NS = 21;
		  //break;
		  //
		  //case 22:
		  //aDisplay.Disp_ORP ("IL ");
		////  if (pH_Interlock_Armed) aDisplay.Disp_pH ("00");
		////  else aDisplay.Disp_pH ("01");
		  //Control_Hold -= 1;
		  //if (Control_Hold <= 0)
		  //{
			  ////Control_Hold = STATE_HOLD_TIME;
			  //NS = 23;
		  //}
		  //else NS = 22;
		  //break;
		  //
		  //case 23:
		  //aDisplay.Disp_ORP ("FLO");
		////  if (Flow_Armed) aDisplay.Disp_pH ("01");
		////  else aDisplay.Disp_pH ("00");
		  //Control_Hold -= 1;
		  //if (Control_Hold <= 0)
		  //{
			  ////Control_Hold = STATE_HOLD_TIME;
			  //NS = 31;
		  //}
		  //else NS = 23;
		  //break;
		  //
		  //case 30:
		  //if (Control_Run_Flag) NS = 31;
		  //else NS = 10;
		  //break;
		  //
		  //case 31:          //------------------ Process pH
		  ////pH_Sensor.Raw (PCBA_CH250K::Spi_Read_pH  ( ));
		  ////pH_Sensor.Check_Safety_Timer (3);
		  ////pH_Sensor.Check_Prop_Mode (3);
		  ////pH_Interlock_Tripped = pH_Interlock_Armed & pH_Sensor.Alarm ( );          // Resets when the alarm goes away
		  ////if (pH_Sensor.Get_Mode ( ) == Off_Mode) {pH_Sensor.Reset_Safety_Timer ( );}
		  ////// sprintf (str, "\r\n pH: %d", (int) pH_Sensor.Current()); PCBA_CH250K::RS232_Send (str);
		 //// aDisplay.Disp_pH (pH_Sensor.Current());
		////  if (ORP_Sensor.Safety_Timer_Tripped)  aDisplay.Disp_ORP ("St ");
		  //NS = 32;
		  //break;
		  //
		  //case 32:                //------------------ Process ORP
		////  ORP_Sensor.Raw (PCBA_CH250K::Spi_Read_ORP ( ));
		  ////ORP_Sensor.Check_Safety_Timer (3);
		  ////ORP_Sensor.Check_Prop_Mode (3);
		  ////if (ORP_Sensor.Get_Mode ( ) == Off_Mode) {ORP_Sensor.Reset_Safety_Timer ( );}
		  ////sprintf (str, "\r\n ORP: %d", (int) ORP_Sensor.Current()); PCBA_CH250K::RS232_Send (str);
		////  aDisplay.Disp_ORP (Util::Bracket (ORP_Sensor.Current ( ), 999.0, 0.0));
		  ////aDisplay.Disp_ORP (ORP_Sensor.Current ( ));
		////  if (pH_Sensor.Safety_Timer_Tripped)   aDisplay.Disp_pH ("St");
		  //NS = 33;
		  //break;
		  //
		  //case 33:
		  ////Flow_Sensor.Raw (PCBA_CH250K::Read_Flow ( ));
		  //// Show_Int ("Read Flow",  PCBA_CH250K::Read_Flow ( ));
		  ////Flow_Sensor.Reset_Safety_Timer ( );                 // Flow never times out
		  ////Flow_Sensor.Set_Mode (Auto_Mode);             // Allow alarms from flow
		  //
		  //// if (Flow_Alarm ( ))
		  ////    {
		  ////    PCBA_CH250K::RS232_Send ("\r\nFlow Chirp!"); //Chirp ( );//----------------------------------------------------------------
		  ////    }
		  //if (!Flow_OK ( )) aDisplay.Disp_ORP ("FLO"); // Show flow status
		  //
		////  if (pH_Interlock_Tripped) aDisplay.Disp_pH ("IL");
		  ////sprintf (str, "\r\n Flow: %d", (int) Flow_Sensor.Current ( )); PCBA_CH250K::RS232_Send (str);
		  //
		  ////if (ORP_Sensor._mode == Auto_Mode)
		  //{
			////  if (ORP_Sensor.Feed ( ) && !pH_Interlock_Tripped && ORP_Sensor.Prop_Feed_Inhibit ( ) && Flow_OK ( ))
			  //aDisplay.LED_Feed (Probe_ORP, true, false);         // Blink LEDs
		  //}
		  ////if (pH_Sensor._mode == Auto_Mode)
		  //{
		////	  if (pH_Sensor.Feed  ( ) && !pH_Interlock_Tripped && pH_Sensor.Prop_Feed_Inhibit ( ) &&  Flow_OK ( ))
			  //aDisplay.LED_Feed (Probe_pH,  true, false);
		  //}
		  //NS = 34;
		  //break;
		  //
		  //case 34:
		  //// if (pH_Sensor.Safety_Timer_Tripped)   aDisplay.Disp_pH ("St");
		  //// if (ORP_Sensor.Safety_Timer_Tripped)  aDisplay.Disp_ORP ("St ");       //
		  //
		  //
		  ////if (ORP_Sensor.Current ( ) <= ORP_Sensor.Get_Lo_Limit ( )) aDisplay.Disp_ORP ("Lo ");
		  ////if (ORP_Sensor.Current ( ) >= ORP_Sensor.Get_Hi_Limit ( )) aDisplay.Disp_ORP ("Hi ");
		  ////if (pH_Sensor.Current ( )  <= pH_Sensor.Get_Lo_Limit ( ))  aDisplay.Disp_pH ("Lo");
		  ////if (pH_Sensor.Current ( )  >= pH_Sensor.Get_Hi_Limit ( ))  aDisplay.Disp_pH ("Hi ");
		  ////
		  ////
		  //alarm = false;
		  ////if (ORP_Sensor.Alarm ( )) { alarm = true;} // aDisplay.Disp_ORP ("AL ");}
		  ////if (pH_Sensor.Alarm ( ))  { alarm = true;} // aDisplay.Disp_pH ("AL");}
		  ////if (Flow_Alarm ( ))        alarm = true;
		  ////    if (alarm) {PCBA_CH250K::RS232_Send ("\r\nGen'l Chirp!"); Chirp ( );} //--------------------------------------------
		  //
		////  if (alarm)  PCBA_CH250K::Digital_Out (DOUT_ALARM, true);  //{DOUT_ALARM, DOUT_SAN_FEED, DOUT_PH_FEED, DOUT_BUZZER};
		////  else PCBA_CH250K::Digital_Out (DOUT_ALARM, false);
		  //
		  ////// Set Feeders -------------------------
		  ////aDisplay.Set_San_Feed (ORP_Sensor.Feed ( ) && !pH_Interlock_Tripped && !ORP_Sensor.Prop_Feed_Inhibit ( ) && Flow_OK ( ));  // Sanitizer feed bit handled through display :(
		  ////if (pH_Sensor.Feed  ( ) && !pH_Interlock_Tripped && !pH_Sensor.Prop_Feed_Inhibit ( ) &&  Flow_OK ( ))
		//////  PCBA_CH250K::Digital_Out (DOUT_PH_FEED,  true);
		  ////else PCBA_CH250K::Digital_Out (DOUT_PH_FEED,  false);
		  //
		  //// Set LED outputs-----------------------
		  ////if (ORP_Sensor.Feed ( ) && !pH_Interlock_Tripped && !ORP_Sensor.Prop_Feed_Inhibit ( ) && Flow_OK ( ))
		  ////aDisplay.LED_Feed (Probe_ORP, true, false);
		  ////else aDisplay.LED_Feed (Probe_ORP, false, false);
		  //
		  ////if (pH_Sensor.Feed  ( ) && !pH_Interlock_Tripped && !pH_Sensor.Prop_Feed_Inhibit ( ) && Flow_OK ( ))
		  ////aDisplay.LED_Feed (Probe_pH, true, false);
		  ////else aDisplay.LED_Feed (Probe_pH, false, false);
		  ////
		  ////// Set Alarms--------------------------
		  ////if (pH_Sensor.Alarm ( ))  aDisplay.LED_Alarm (Probe_pH, true, false);  else aDisplay.LED_Alarm (Probe_pH, false, false);
		  ////if (ORP_Sensor.Alarm ( )) aDisplay.LED_Alarm (Probe_ORP, true, false); else aDisplay.LED_Alarm (Probe_ORP, false, false);
		  //
		  //NS = 30;
		  //break;
		  //
		  //case 99:
		////  PCBA_CH250K::RS232_Send ("Internal error. Restarting");
		  //NS = 0;
		  //default:
		  //
		  //NS = 99;
		  //break;
	  //}
	  //return NS;
  //} 
