/*
 * Sensor.h
 *
 * Created: 8/16/2017 13:08:30
 *  Author: Fredc
 */ 

#include "Config.h"

#ifndef SENSOR_H_
#define SENSOR_H_

enum Probe_Type {Probe_None, Probe_pH, Probe_ORP, Probe_PPM, Probe_TDS, Probe_TEMP, Probe_Flow};
//enum Run_Mode   {Off_Mode, Man_Mode, Auto_Mode};
class CSensor 
   {
   public: 
      CSensor ( ); 
      CSensor (Probe_Type Probe);
      
      CSensor (Probe_Type Probe, double default_gain, double default_offset,
                  double default_setpoint, double default_hi_limit, double default_lo_limit);
       
           
      void Raw (int raw);              // Input ADC Counts
      double Current (void);           // Get Current value 
   
      //Run_Mode Cycle_Mode (void);      // 
      //Run_Mode Get_Mode (void);        //
      //Run_Mode Set_Mode (Run_Mode mode);   //
      
      double Get_Set_Point (void);
      void   Set_Set_Point (double Set_point);
      double Inc_Set_Point (double incr, double MAX, double MIN);
         
      double Get_Hi_Limit  (void);
      void   Set_Hi_Limit  (double Hi_Limit);
      double Inc_Hi_Limit  (double incr, double MAX, double MIN);
      
      double Get_Lo_Limit  (void);
      void   Set_Lo_Limit  (double Lo_Limit);
      double Inc_Lo_Limit  (double incr, double MAX, double MIN);
           
      //double Get_Safety_Time_Limit (void);
      //void   Set_Safety_Time_Limit (double n);
      //double Inc_Safety_Time_Limit (double incr);
      //bool   Safety_Timer_Tripped;
      //void   Reset_Safety_Timer (void);
      //void   Check_Safety_Timer (int n);

     //double Get_Prop_Limit  (void);
     //void   Set_Prop_Limit  (double Prop);
     //double Inc_Prop_Limit  (double incr);
     //void   Check_Prop_Mode (int n);            // Clock for proportional time
     //bool   Prop_Feed_Inhibit (void);           // Proportional Mode is inhibiting feed
     
     void   Set_Cal_Target (double Cal);
     double Get_Cal_Target (void);
     double Inc_Cal_Target (double incr, double MAX, double MIN);
     void   Do_Calibration (void);
     
     int   Get_Status (void);             // Get Status Nybble
      
    bool    Alarm (void);                  // true parameter out of bounds
    bool    Feed (void);                   // true if feed required
      
    void    Set_Reverse_Feed (bool reverse);   //
    bool    Get_Reverse_Feed (void);

   // void    Set_Default_Parameters (void);
    
     
   protected:
   
   private:
      Probe_Type _probe;
      //Run_Mode   _mode;
      
      //bool   Reverse_Feed;             // True for acid feed mode
      //bool   In_Dead_Zone (void);
      //bool   Current_Feed;
      //bool   Auto_Feed (void);        // Decides if we should feed when in auto
      //bool   Prop_Inhibit;             // Proportional control is inhibiting feed
      
      double Current_Value;
      double UnCal_Value;
      double SetPoint;
      double HiLimit;
      double LoLimit;
      
      //bool   Prop_Feed (void);      // true if Proportional OK to feed
      //int    Prop_Time;             // Proportional timer
      //double Prop_Time_Limit;       // Proportional Limit
      //bool   In_Prop_Time_Zone (void);
      //bool   In_Prop_Feed_Zone (void);

      //int    Safety_Time;           // Safety time in seconds
      //double Safety_TimeLimit;      // In minutes
      
      double Gain;
      double Offset;
      double Cal_Target;

   };



#endif /* SENSOR_H_ */

