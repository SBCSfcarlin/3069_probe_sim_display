/*
 * PCBA_SIM18.h
 *
 * Created: 7/10/2018 11:38:40
 *  Author: Fredc
 * These are the routines for the Bread board for the Simulator
       Breadboard contains a ATMega 644PA-PU
	   
      Contains classes for
      PCB2018 IO, timer
      Watchdog
      Analog-Digital Conversion
      Pulse Wave Modulators
      Serial Comm Ports
      SPI Bus
 */ 


  #ifndef PCBA_SIM18_H_
  #define PCBA_SIM18_H_


  #include "Util.h"
 // #include "Config.h"

 // #include "Controller.h"

  #ifndef  __AVR_ATmega644PA__
  #error Not ATmega644 proc
  #endif
  


  class PCBA2018 
      {
	  public:

		  static void IO_Init (void);                     // Initialize IO
		  static void Timer_Init (void);                  // Initialize timer
       
		  static bool Get_One_Second_Flag (void);              // One second Timer
		  static bool Get_Timer_Tick (void);              // 2 ms Tick
	  
		  static void Test (int t);                       // A test

	  protected:

	  private:
   
       } ;
  //----------------------------------------------------------------------------------------  
  enum Button {NONE, UP, DOWN, LEFT, RIGHT, OK, MYSTERY};
  class Buttons
     {
     public:
        static Button Read_Buttons ( );
     protected:

     private:

     };
  //--------------------------------------------------------------------------------------------

  class Watch_Dog
     {
     public:
        static void Init  (void);// III:138
        static void Touch (void);
        static void Off   (void);
        static void Reboot  (void);

     protected:

     private:
     			  
     };
  //-------------------------------------------------------------------------------------
  class Analog_Converter
     {
     public:
        static void Init (void);                    // Initialize IO
        static void Start (int chan, bool High_Range);
        static bool Done ( );
        static int  Read ( );
     };

  //---------------------------------------------------------------------------------
  class Pulse_Wave_Modulator
     {
     public:
        void Init (void);                    // Initialize IO
        void Set (int pwm);                  //
     };

//----------------------------------------------------------------------------    
  class Serial_Port
	     {
		  public:
			  void Init (int port, int baud_rate, int buffer_size);
           void Tx_Check ( );
			  void Rx_Check ( );

           void Send (char ch);                    // Send a char
			  void Send (char *ptr);                  // Send a line
           char Get_Char ( );                      // Returns next char or null  

		  protected:

		  private:
           CBox  Rx_Buff;
           CBox  Tx_Buff;
           int hdw_port;
           void UART_init (int port, int baud_rate);      
	     };




     //=========================================================================
class Rho_Output
   {
   public:
      static void Init (void);
      static void Test (bool level);        // Strobe Rho
      static void Set_FreqA (double freq);    // Set freq Hz
      //static void Set_FreqB (double freq);    //
      static double Get_FreqA (void);    // Set freq Hz
      //static double Get_FreqB (void);    //

      

   protected:

   private:
      static void RhoA (bool A);          // Set output
      //static void RhoB (bool B);
      //static int RhoA_Count;
      //static int RhoB_Count;
      //static int RhoA_Freq;
      //static int RhoB_Freq;
      //static bool RhoA_Out;
      //static bool RhoB_Out;
   };
 

#endif /* PCBA_SIM18_H_ */


