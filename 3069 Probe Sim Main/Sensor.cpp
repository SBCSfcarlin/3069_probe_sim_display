/*
 * Sensor.cpp
 *
 * Created: 8/16/2017 13:08:13
 *  Author: Fredc
 */ 
#include "Config.h"
#include "Sensor.h"
#include "Util.h"

  
   CSensor::CSensor ( ) 
      { };
   
   CSensor::CSensor (Probe_Type Probe, double default_gain, double default_offset,
                     double default_setpoint, double default_hi_limit, double default_lo_limit)
      {
       _probe  = Probe;                    // Set up the probe
       Gain = default_gain;    
       Offset = default_offset;
       SetPoint = default_setpoint;
       HiLimit  = default_hi_limit;
       LoLimit  = default_lo_limit;   
      };
      
   void CSensor::Raw (int raw)         // Reads ADC and updates current val
      {
      double ADC_Counts;
         
      ADC_Counts    = (double) raw;
      UnCal_Value   = ADC_Counts * Gain;
      Current_Value = UnCal_Value + Offset;
      };
      
   double CSensor::Current (void) {return Current_Value;}
 
 //---------------------------------------------
   
   //Run_Mode CSensor::Get_Mode (void) {return _mode;} // Off_Mode, Man_Mode, Auto_Mode 
        //
   //Run_Mode CSensor::Set_Mode (Run_Mode mode) {_mode = mode; return _mode;}
      //
   //Run_Mode CSensor::Cycle_Mode (void)     // Off_Mode, Man_Mode, Auto_Mode
      //{
      //Run_Mode next;
      //next = Off_Mode;    
      //switch (_mode)
         //{
         //case Off_Mode:  next = Man_Mode;  break;
         //case Man_Mode:  next = Auto_Mode; break;
         //case Auto_Mode: next = Off_Mode; break;
         //}
       //_mode = next;    
      //return _mode;   
      //}
  //-----------------------------------------------------    
   //bool   CSensor::Alarm ( )                   // true if alarm in progress
         //{
         //if (_mode == Auto_Mode)             // IV:5
            //{
            //if (Current_Value >= HiLimit) return true;
            //if (Current_Value <= LoLimit) return true;
            //if (Safety_Timer_Tripped)     return true;  
            //}
         //return false;
         //}
         //
   //bool CSensor::Feed (void)                   // true if feed in progress
      //{
      //switch (_mode)          //{Off_Mode, Man_Mode, Auto_Mode}; 
         //{
         //case Off_Mode: 
            //return false; 
            //break;
         //case Man_Mode:       // Always feed if not timed out
            //if (!Safety_Timer_Tripped)
               //return true;
            //break;
         //case Auto_Mode: 
             //if (!Safety_Timer_Tripped ) //            && Prop_Feed ( ))
                //return Auto_Feed ( ); 
            //break;
         //}
      //return false;
      //}
      
    //bool CSensor::Auto_Feed (void)          // Decides if we should feed when in auto
       //{
       //bool CF;
       //bool DZ;
       //bool AgT;
       //
       //CF = Current_Feed;
       //DZ = In_Dead_Zone ( );
       //AgT = (Current_Value > SetPoint);        // Actual Greater than Target
       //
       //if (Reverse_Feed)                        // If we are feeding
          //Current_Feed = (AgT) && (!DZ || CF); 
          //else
          //Current_Feed = (!AgT) && (!DZ || CF);
       //return Current_Feed;  
       //}
       
    //int CSensor::Get_Status (void)            // Get Status Nybble
       //{
       //int A;
       //
       //
       //switch (_mode)   
          //{
          //case Off_Mode:  A = 0; break;
          //case Man_Mode:  A = 1; break;
          //case Auto_Mode: A = 2; break; 
          //default: A = 0;        break;
          //}
       //if (Feed ( ))  A += 4;
       //if (Alarm ( )) A += 8;
       //return A;
       //}
       //
    //void CSensor::Check_Prop_Mode (int n)             // Check once per second.
        //{   
        //Prop_Time += n;                               // Add this seconds to Prop_Time 
        //if (Prop_Time >= 100) Prop_Time = 0;          // Wrap around
        //}
                   //
    //bool CSensor::In_Prop_Time_Zone (void)
      //{
      //if (Prop_Time > (100 - Prop_Time_Limit)) return true;
          //return false;
       //} 
        //
    //bool CSensor::In_Prop_Feed_Zone (void)
      //{
      //double PFA;                // Prop Feed Area
      //double PFL;                // Prop Feed Limit
      ////double P;                  // Percentage of set point
      //bool ITZ;                  // In the Zone
     //
      //ITZ = false;
      //if (Prop_Time_Limit == 0) return false;
      //
      //PFA = (double) Prop_Time_Limit * SetPoint / 100.0;
      //
      //if (Reverse_Feed) PFL = SetPoint + PFA;        else PFL = SetPoint - PFA;
      //if (Reverse_Feed) ITZ = (Current_Value < PFL); else ITZ = (Current_Value > PFL); 
       //
      //return ITZ;
      //}
      //
    //bool CSensor::Prop_Feed_Inhibit (void)                    // True if inhibiting
      //{
      //if (In_Prop_Feed_Zone ( ) && In_Prop_Time_Zone ( )) return true;  
      //return false; 
      //}
  //
   //bool   CSensor::In_Dead_Zone (void) 
      //{
      //double P;                     // Percentage of set point
      //
      //if (Reverse_Feed)
         //P = (Current_Value - SetPoint) / (Current_Value);
         //else
         //P = (SetPoint - Current_Value) / (Current_Value);
      //
      //if ((P >= 0) && (P <= DEFAULT_DEAD_ZONE)) return true;
      //return false;
      //}
      //
   //Run_Mode CSensor::Mode ( ) {return _mode;}
            
   void   CSensor::Set_Cal_Target (double Target) {Cal_Target = Target;}
   double CSensor::Get_Cal_Target (void)          {return Cal_Target;}
   double CSensor::Inc_Cal_Target (double incr, double MAX, double MIN)   {Cal_Target += incr; 
                                   return Util::Bracket (Cal_Target, MAX, MIN);}
   void   CSensor::Do_Calibration  (void)         {Offset = Cal_Target - UnCal_Value;}
   
   double CSensor::Get_Set_Point (void) {return SetPoint;}   
   void   CSensor::Set_Set_Point (double Set_point) {SetPoint = Set_point;}
   double CSensor::Inc_Set_Point (double incr, double MAX, double MIN) {SetPoint = Util::Bracket (SetPoint + incr, HiLimit, LoLimit); 
                                  return Util::Bracket (SetPoint, MAX, MIN);}
     
  double CSensor::Get_Hi_Limit  (void) {return HiLimit; }   
  void   CSensor::Set_Hi_Limit  (double Hi_Limit)  {HiLimit  = Hi_Limit; }
  double CSensor::Inc_Hi_Limit  (double incr, double MAX, double MIN) {HiLimit  = Util::Maximum (HiLimit + incr, SetPoint); 
                                return Util::Bracket (HiLimit, MAX, MIN);}
     
  double CSensor::Get_Lo_Limit  (void) {return LoLimit; }
  void   CSensor::Set_Lo_Limit  (double Lo_Limit)  {LoLimit  = Lo_Limit;}
  double CSensor::Inc_Lo_Limit  (double incr, double MAX, double MIN) {LoLimit = Util::Minimum (LoLimit + incr, SetPoint); 
                                 return Util::Bracket (LoLimit, MAX, MIN);};
  
 //double CSensor::Get_Prop_Limit  (void) {return Prop_Time_Limit; } 
 //void   CSensor::Set_Prop_Limit  (double n) {Prop_Time_Limit = n; } 
 //double CSensor::Inc_Prop_Limit  (double incr) {Prop_Time_Limit += incr; return Prop_Time_Limit;} 
     //
 //void  CSensor::Set_Reverse_Feed (bool reverse) {Reverse_Feed = reverse; }
 //bool  CSensor::Get_Reverse_Feed (void) {return Reverse_Feed; }
  
//double CSensor::Get_Safety_Time_Limit  (void) {return Safety_TimeLimit; }
//void   CSensor::Set_Safety_Time_Limit  (double n) {Safety_TimeLimit = n;}  
//double CSensor::Inc_Safety_Time_Limit  (double incr) {Safety_TimeLimit += incr; return Safety_TimeLimit;} 
//void   CSensor::Reset_Safety_Timer ( ) {Safety_Time = 0;} 
  
  
  //void CSensor::Check_Safety_Timer (int n)            // Bump once per second. 
     //{
     //int limit;
        //
     //if (_mode == Off_Mode) Safety_Time = 0; // Reset safety timer
     //if (_mode != Off_Mode)                  // Don't screw with safety timer if 
        //{                                    //  sensor off
        //limit = 60 * (int) Safety_TimeLimit;
        //if (Feed ( )) Safety_Time += n;          // Accumulate whilst feeding
           //else Safety_Time = 0;             // Restart then feed off
        //if ((Safety_Time >= limit) && ((int) Safety_TimeLimit > 0))      // 0 diables timer
           //{
           //Safety_Timer_Tripped = true;
           //Safety_Time = limit;                 // Keep Safety time in bounds
           //}            
           //else 
           //Safety_Timer_Tripped = false;
        //}  
     //}

    
  //void CSensor::Set_Default_Parameters ( )
     //{
     //switch (_probe)
        //{
         //case Probe_pH:
            //SetPoint = pH_DEFAULT_SETPOINT;
            //HiLimit  = pH_DEFAULT_HI_LIMIT;
            //LoLimit  = pH_DEFAULT_LO_LIMIT;
            ////Safety_TimeLimit = pH_DEFAULT_SAFETY_TIME;
           //// Safety_Timer_Tripped = false;
           //// Safety_Time = 0;
           //// Prop_Time_Limit = DEFAULT_PROP_LIMIT;
           //// Cal_Target  = pH_DEFAULT_CAL_TARGET;
            //Gain        = pH_DEFAULT_GAIN;
            //Offset      = pH_DEFAULT_OFFSET;
            //break;
             //
       //case Probe_ORP:
          //SetPoint = ORP_DEFAULT_SETPOINT;
          //HiLimit  = ORP_DEFAULT_HI_LIMIT;
          //LoLimit  = ORP_DEFAULT_LO_LIMIT;
         //// Safety_TimeLimit = ORP_DEFAULT_SAFETY_TIME;
          ////Safety_Timer_Tripped = false;
        ////  Safety_Time = 0;
        ////  Prop_Time_Limit = DEFAULT_PROP_LIMIT;
          //Gain     = ORP_DEFAULT_GAIN;
          //Offset   = ORP_DEFAULT_OFFSET;
          //break;
       //
      //case Probe_Flow:
         //SetPoint = Flow_DEFAULT_SETPOINT;
         //HiLimit  = Flow_DEFAULT_HI_LIMIT;
         //LoLimit  = Flow_DEFAULT_LO_LIMIT;
        //// Safety_TimeLimit = 600;
        //// Safety_Timer_Tripped = false;
        //// Safety_Time = 0;
       ////  Prop_Time_Limit = DEFAULT_PROP_LIMIT;
         //Gain     = Flow_DEFAULT_GAIN;
         //Offset   = Flow_DEFAULT_OFFSET;
         //break;
       //}
    //}       
       
            
         
       
         
        