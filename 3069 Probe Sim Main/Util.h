/*
 * Util.h
 *
 * Created: 6/2/2017 11:51:31
 *  Author: Fredc
 */ 


#ifndef UTIL_H_
#define UTIL_H_

 //#define uchar uint8_t
 //#define uint  uint16_t
 // typedef  uint8_t   uchar 
 // typedef  uint16_t  uint  

 // int8_t
 // int16_t
//-----------------------------------------------------------------
// Data types
//
//typedef uint8_t byte;

//typedef uint8_t byte;
//typedef uint8_t bool;
/*
 (signed/unsigned) char - 1 byte
 (signed/unsigned) short - 2 bytes
 (signed/unsigned) int - 2 bytes
 (signed/unsigned) long - 4 bytes
 (signed/unsigned) long long - 8 bytes
 float - 4 bytes (floating point)
 double - alias to float


 C99 standard available to GCC. 
 These use the convention of a "u" to denote the signedness (no "u" to denote signed), 
 "int" to denote that it's a integer and not a float, 
 the number of bits in the int and a trailing "_t". 
 Examples:


 Code:
 int8_t - Signed Char
 uint16_t - Unsigned Int
 uint32_t - Unsigned Long
 int64_t - Signed Long Long

 */
//---------------------------------------------------------------
//
// Standard bit definitions
//
#define BIT_0					0x01
#define BIT_1					0x02
#define BIT_2					0x04
#define BIT_3					0x08
#define BIT_4					0x10
#define BIT_5					0x20
#define BIT_6					0x40
#define BIT_7					0x80
//----------------------------------------------------------------

class Util
   {
   public:
      int    static Bracket (int x,    int high,    int low);  
      double static Bracket (double x, double high, double low);
      double static Minimum (double x, double min);
      double static Maximum (double x, double max);
      char   static ToUpper (char c);
      int    static Abs     (int A);

     static char * Debug_String;
     static bool Debug_Flag;               

   protected:

   private:

   };


class CBox
   {
   public: 
      ~CBox (void);

      void CBox_Init (int b_size);
      int  Put_Char (char val);  // Returns incounter
      char Get_Char (void);      // Returns byte
      bool Ready    (void);      // Returns data ready flag

   private:
      char *buffer;
      int buffer_size;
      int incounter;
      int outcounter; 
   };


#endif /* UTIL_H_ */