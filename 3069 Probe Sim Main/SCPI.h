/*
 * SCPI.h
 *
 * Created: 7/9/2018 12:26:06
 *  Author: Fredc
 */ 


#ifndef SCPI_H_
#define SCPI_H_


  #include "Controller.h"
 //#include "Util.h"


 #include "PCBA_SIM18.h"
 
  enum SCPI_Command
      {
	  SCPI_CHEMTROL,
	  SCPI_IDN,
	  SCPI_OPC,
	  SCPI_CLS,
	  SCPI_PH,
	  SCPI_ORP,
	  SCPI_SANITIZER,
	  SCPI_PH_SETPOINT,
	  SCPI_PH_HILIMIT,
	  SCPI_PH_LOLIMIT,
	  SCPI_PH_SAFETY,
	  SCPI_PH_PROPORTIONAL,
	  SCPI_ORP_SETPOINT,
	  SCPI_ORP_HILIMIT,
	  SCPI_ORP_LOLIMIT,
	  SCPI_ORP_SAFETY,
	  SCPI_ORP_PROPORTIONAL,
	  SCPI_PPM_SETPOINT,
	  SCPI_PPM_HILIMIT,
	  SCPI_PPM_LOLIMIT,
	  SCPI_PPM_SAFETY,
	  SCPI_PPM_PROPORTIONAL,
	  SCPI_PUMP,
	  SCPI_FEED,
	  SCPI_INTERLOCK,
	  SCPI_FLOW
     };
	 
	 
 class CSCPI
    {
	   
	public: 
	
	  bool   SCPI_Chemtrol_Flag;
	  bool   SCPI_ORP_Flag;
	  bool   SCPI_pH_Flag;
	  bool   SCPI_PPM_Flag;
	  
	  double SCPI_Accumulator;
	  bool   SCPI_Decimal;
	  double SCPI_Factor;
	   
	  void   SCPI_Init     (void);
	  //int    SCPI_Eat_Char (char C, char E, int M, int Q, int S);
	  
	  void   SCPI_Process  (char *str);
	  // void  SCPI_Process  (char *buff)
	  
	        void   SCPI_Put_pH           (double pH);
	        void   SCPI_Put_Feed         (void);
	        void   SCPI_Set_Feed         (void);
	        void   SCPI_Put_Mode         (void);
	        void   SCPI_Set_Mode         (void);
	        void   SCPI_Put_ORP          (double ORP);
	        void   SCPI_Put_PPM          (double PPM);
	        void   SCPI_Put_Flow         (void);
	        void   SCPI_Set_Flow         (void);
	        void   SCPI_Put_LOLIMIT      (void);
	        void   SCPI_Set_LOLIMIT      (void);
	        void   SCPI_Put_Status       (void);
	        void   SCPI_Put_HILIMIT      (void);
	        void   SCPI_Set_HILIMIT      (void);
	        void   SCPI_Put_IDN          (void);
	        void   SCPI_Put_Setpoint     (void);
	        void   SCPI_Set_Setpoint     (void);
	        void   SCPI_Put_Safety       (void);
	        void   SCPI_Set_Safety       (void);
	        void   SCPI_Put_Proportional (void);
	        void   SCPI_Set_Proportional (void);
	        void   SCPI_Put_Interlock    (void);
	        void   SCPI_Set_Interlock    (void);
	      
	       
	protected:
	
	private:
      int    SCPI_Eat_Char (char C, char E, int M, int Q, int S);
	   bool   SCPI_Accumulate       (char C);
	   void   SCPI_Accumulate_Init  (void);
    };
 #endif /* SCPI_H_ */