/*
 * 3069 Probe Sim Main.cpp
 *
 * Created: 7/6/2018 13:39:20
 * Author : Fredc
 */ 

#include <avr/io.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "Controller.h"

CController aCont;

int main (void)
   {
   aCont.Cont_Init ( );
   sei ( );

   while (true)
	   {
	   aCont.Cont_Execute ( );
	   }
   }
