/*
 * SPI.cpp
 *
 * Created: 11/6/2018 10:05:19
 *  Author: Fredc
 */
 #include "SPI.h" 
 #include "Util.h"
  #define F_CPU 8000000
  #include <util/delay.h>
 #include <avr/io.h>

 // ===================SPI BUS=================================================
  //#define SPI_BUFF_MAX 20
  //static int  Spi_Buff_Count;
  //static char Spi_Buff [SPI_BUFF_MAX];

   //FROM 128 data sheet  (GENERIC CODE)
   //#define SPI_DDR	 DDRB	  // The spi DDR
   //#define SPI_PORT	  PORTB	  // The spi PORT

   #define DD_SS     4 //0
   #define DD_SCK    7 //1
   #define DD_MOSI   5 //2
   #define DD_MISO   6  //3
   
   #define SPI_DELAY_US 100.0

   
   void SPI_BUS::Init (void)
      {
      DDRB  |= 0xb0;                   // Set MOSI and SCK and SS output (was 0x07) VI:84
                                       // Bit 0 is our local busy
      //PORTB |= 0x01;                   // Set done flag SS high?         (was 01)

      PORTB |= 0x10;                   // Set PLUP until hardware fixed
      PORTB &= ~0x01;            // Clear ready
      PRR0 = 0;                  // Power reduction Bit 0x04 in partic p66
      //// Enable SPI, Master, set clock rate fck/128
      SPCR = 0x53;    // (1 << SPE) | (1 << MSTR) | (1 << SPR1) | (1 << SPR0);  //spi mode 0, 62kHz
      //SPCR = 0x57; //NC  
      //SPCR = 0x5b; // All 1s 
      //SPCR = 0x5f; // NC
//
      ////SPCR =  (1 << SPE) | (1 << MSTR) | (1 << SPR1) | (1 << SPR0) | (1 << CPOL);  //spi mode 2
      ////SPCR =  (1 << SPE) | (1 << MSTR) | (1 << SPR1);   // Enable SPI, Master, set clock rate fck/64    
      ////SPCR =  (1 << SPE) | (1 << MSTR) | (1 << SPR0);   // Enable SPI, Master, set clock rate fck/16
      SPSR = 0x00;            // No 2x clock
      }
      

    bool SPI_BUS::Ready ( )
       {
      // if ((SPSR & 0x80) != 0) return true;
       if ((PORTB & 1)   != 0) return true;  // was 0x01 local busy do we need this?
       return false;
       }
 //
   //int  SPI_BUS::Read_pH (void)
      //{
      //int ck;
      //int phhi;
      //int phlo;
      //int pH;
 //
//
      //ck = Transmit (SPI_READ, 0, 202, 2);
  ////    phhi = (int) Get (0);
 ////     phlo = (int) Get (1);
//
//
      //pH = (phhi << 8) | phlo;
      //pH & 0x3ff;
      //return  pH;
      //}
//
////-------------------------------------------------------
//
     //
   //int  SPI_BUS::Read (int addr, int offset, int byte_count)
      //{
      //int ck;                 // 199 Version
      //int hi;                 // 202 Data
      //int lo;
      //int val;
//
      ////ck = PCBA_CH250K::Spi_Transmit (SPI_READ, 1, 202, 2);
     //// ck = Transmit (SPI_READ, 1, 202, 2);      // III:137 
        //ck = Transmit (SPI_READ, addr, offset, byte_count, 0);      // III:137 ///ty=ry 10 instead of 2 here
//
      //hi  = (int) Get (0);     // SPI_BUS::Get
      //lo  = (int) Get (1);
      //val = (hi << 8) | lo;
      //val &= 0x3ff;
      //return val;
      //}

    //

    //void SPI_BUS::Debug (char *str)
    //{
       //while (Util::Debug_Flag == false)
       //{
          //Util::Debug_String = str;
          //Util::Debug_Flag = true;
       //}
    //}

    int  SPI_BUS::Read (int addr, int offset)
       {
       int ck;                 // 199 Version
       int hi;                 // 202 Data
       int lo;
       int val;
       int i;
            

       //COM_1.Send ("ZZ");

      // #define uchar uint8_t
      // #define uint  uint16_t

      //uchar test;
      // uint test2;

      //ck = PCBA_CH250K::Spi_Transmit (SPI_READ, 1, 202, 2);
      // ck = Transmit (SPI_READ, 1, 202, 2);      // III:137
     
      for (i = 0; i < 10; i++)
         {
         ck = Transmit (SPI_READ, addr, offset, 2, 0);      // III:137 ///try 10 instead of 2 here

         hi  = (int) Get (0);     // SPI_BUS::Get
         lo  = (int) Get (1);
         val = (hi << 8) | lo;
         //val &= 0x3ff;
         if ( val != -1) break;
         }
      return val;
      }
    //

    int  SPI_BUS::Read_Byte (int addr, int offset)
       {
       int ck;                 // 199 Version
       int hi;                 // 202 Data
       int lo;
       int val;


       //ck = PCBA_CH250K::Spi_Transmit (SPI_READ, 1, 202, 2);
       // ck = Transmit (SPI_READ, 1, 202, 2);            // III:137
       ck = Transmit (SPI_READ, addr, offset, 1, 0);      // III:137
       hi  = (int) Get (0);     // SPI_BUS::Get
       lo  = (int) Get (1);
       val = (hi << 8) | lo;
       val = lo;
       //val &= 0xff;
       return val;
       }
 
 
    int SPI_BUS::Write (int addr, int offset, int byte_count, int value)
      {
       int ck;                 // 199 Version

       //ck = PCBA_CH250K::Spi_Transmit (SPI_READ, 1, 202, 2);
       // ck = Transmit (SPI_READ, 1, 202, 2);      // III:137
       // force byte count to 2 here---------
       ck = Transmit (SPI_WRITE, addr, offset, byte_count, value);      // III:137
       return ck;
      }
 //
    int SPI_BUS::Send (int cData)
       {
       // Start transmission assume ready
       PORTB &= ~0x01;            // Clear ready
       SPDR   = cData & 0xff;
       // Wait for transmission complete
       while ((SPSR & 0x80) == 0) //! (SPSR &  (1 << SPIF) ) )
          ; 
       PORTB |=  0x01;            // Reset ready
       return SPDR;
       }
        //

   #define SPI_IDLE 0x34

   int SPI_BUS::Transmit (Spi_Command SPI_C, int Spi_Address, int Spi_Offset, int byte_count, int value)
       {
       //char *SBP;                   //Spi_Buff_Pointer
       int Spi_Buff_chksum;
       int  i;
       int val;
       
       
       Spi_Buff_Count = Util::Bracket (byte_count, SPI_BUFF_MAX, 0);   // Bracket count

       switch (SPI_C)
          {
          case SPI_READ:   
             for (i = 0; i < Spi_Buff_Count; i++) 
                Spi_Buff [i] = 0x00;              // Clear buffer 
             Send (0xa1);                         // Start off checksum
             Spi_Buff_chksum = 0xa1; 
             break;   

          case SPI_WRITE:
             Spi_Buff [0] = (value >> 8) & 0xff; // Maybe should be 3F
             Spi_Buff [1] = value & 0xff; 
             Send (0xa2); 
             Spi_Buff_chksum = 0xa2;                  // Start off checksum
             break; 

          default:        
             Send (0xa1); 
             Spi_Buff_chksum = 0xa1; 
             break;
          } 

       sdly ( );   
       Send (Spi_Address);    Spi_Buff_chksum += Spi_Address;    sdly ( );
          //sprintf (str, "\r\nSpi address: %x", Spi_Address); Debug (str);
       Send (Spi_Offset);     Spi_Buff_chksum += Spi_Offset;     sdly ( );     //ptr = offset to EEPROM data
           //sprintf (str, "\r\nSpi offset: %x", Spi_Offset); Debug (str);
       Send (Spi_Buff_Count); Spi_Buff_chksum += Spi_Buff_Count; sdly ( );  
          //sprintf (str, "\r\nSpi count: %x", Spi_Buff_Count); Debug (str);                          
       sdly ( );

       Spi_Buff_Pointer = Spi_Buff;       // &gEEdata [0];
      // SBP = Spi_Buff;

       i = 0;
       while (i < Spi_Buff_Count) 
          {
          //*Spi_Buff_Pointer = Send (Spi_Buff_chksum);
          // Spi_Buff_chksum += *Spi_Buff_Pointer;  
           switch (SPI_C)
              {
              case SPI_READ:  Spi_Buff [i] = Send (SPI_IDLE); break;                 
              case SPI_WRITE: Send (Spi_Buff [i]); break;
              }
           Spi_Buff_chksum += Spi_Buff [i]; 
           sdly ( );
           sdly ( );
           i += 1;
          }
         //sprintf (str, "Spi Data: %x %x ", Spi_Buff [0], Spi_Buff [1]); Debug (str);
        //val = Send ((uchar) SPI_IDLE);
      val = Send (Spi_Buff_chksum);
      if (val != Spi_Buff_chksum)
         {
         i = 0; //checksum fail
         }
         else
         {
         i = 1;
         }
       //return (i);
       //Send (SPI_IDLE);                // Flush the t
       //sdly ( );
       //Send (SPI_IDLE);                // Flush the t
       //sdly ( );
       //Send (SPI_IDLE);                // Flush the t
       //sdly ( );
       return (Spi_Buff_chksum);
	   //return 0;
      }  
   //-----------------------------------------------
   int SPI_BUS::Get (int index)
      {
      //if (index == 0) return 0xaa;
      //if (index == 1) return 0x55;
      return Spi_Buff [index];
      }
      //

   //void SPI_BUS::Put (int chan1, int chan2, int chan3)
      //{
      //Spi_Buff [0] = (chan1 >> 8) & 0xff;    // Big endian
      //Spi_Buff [1] = chan1 & 0xff;
      //Spi_Buff [2] = (chan2 >> 8) & 0xff;
      //Spi_Buff [3] = chan2 & 0xff;
      //Spi_Buff [4] = (chan3 >> 8) & 0xff;
      //Spi_Buff [5] = chan3 & 0xff;
      //return;
      //}
   //
  //
  void SPI_BUS::sdly (void)
     {
     unsigned char c;

     //for (c = 1;  c < 400;  c++)
     //   ;
        _delay_us (SPI_DELAY_US); //
     }
 //